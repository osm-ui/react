[![License: MIT](https://img.shields.io/badge/license-MIT-blue.svg)](https://opensource.org/licenses/MIT)
[![pipeline status](https://gitlab.com/osm-ui/react/badges/develop/pipeline.svg)](https://gitlab.com/osm-ui/react/commits/develop)
[![coverage report](https://gitlab.com/osm-ui/react/badges/develop/coverage.svg)](https://gitlab.com/osm-ui/react/commits/develop)

# OSM User Interface for React

The OSM User Interface initiative aims to provide knowledge and tools in order to build awesome OSM tools.


**This project is heavy development phase, don't use it in production until the first 1.0.0 stable version.**


## Getting started

The components are visible here: https://osm-ui.gitlab.io/react


## Contribute

See [CONTRIBUTING.md](CONTRIBUTING.md)
