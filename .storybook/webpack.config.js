const path = require('path');

module.exports = {
  module: {
    rules: [
      {
        test: /\.css$/,
        loaders: ['style-loader?sourceMap', 'css-loader'],
        include: path.resolve(__dirname, '..')
      },
      {
        test: /\.(png|jpg|gif|svg|woff|woff2|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'file-loader',
        include: path.resolve(__dirname, '..'),
        query: {
          name: 'assets/[name].[ext]?[hash]'
        }
      },
      {
        test: require.resolve('leaflet'),
        loader: 'expose-loader?L',
        include: path.resolve(__dirname, '..')
      }
    ]
  }
};
