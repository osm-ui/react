import React from 'react';
import PropTypes from 'prop-types';
import { Notification, Button } from 'index';

class FakeNotificationCenter extends React.Component {
  static propTypes = {
    children: PropTypes.node.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      notifications: this.findAllNotifs(this.props.children)
    };
  }

  componentWillReceiveProps() {
    this.resetNotifications();
  }

  resetNotifications = () => {
    this.setState(
      {
        notifications: null
      },
      () =>
        this.setState({
          notifications: this.findAllNotifs(this.props.children)
        })
    );
  };

  findAllNotifs = components => {
    return React.Children.map(components, component => {
      if (component.type === Notification) return component.props.id;
      else return this.findAllNotifs(component.props.children);
    });
  };

  addTimeout = component => {
    const notifications = this.state.notifications;

    if (notifications) {
      if (component.type === Notification)
        if (notifications.includes(component.props.id))
          return React.cloneElement(component, {
            onTimeoutClose: () =>
              this.setState({
                notifications: notifications.filter(
                  element => element !== component.props.id
                )
              })
          });
        else return null;
      else
        return React.cloneElement(component, {
          children: React.Children.map(
            component.props.children,
            this.addTimeout
          )
        });
    }
    return null;
  };

  render() {
    const button = (
      <Button onClick={this.resetNotifications}>Restart Notifications</Button>
    );

    return (
      <div>
        {React.Children.map(this.props.children, this.addTimeout)}
        {button}
      </div>
    );
  }
}

// FakeNotificationCenter.propTypes = {};
// FakeNotificationCenter.defaultProps = {};
FakeNotificationCenter.displayName = 'FakeNotificationCenter';

export default FakeNotificationCenter;
