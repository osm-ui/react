export function makePrefix(style) {
  return `fa${style[0]}`;
}

export function makeName(name) {
  return `fa-${name}`;
}
