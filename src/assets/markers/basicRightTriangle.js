import raw from '!!raw-loader!./basicRightTriangle.svg';
import component from '!!svg-react-loader!./basicRightTriangle.svg';

export default {
  iconAnchor: [25, 25],
  raw,
  component
};
