import raw from '!!raw-loader!./pointerClassic.svg';
import component from '!!svg-react-loader!./pointerClassic.svg';

export default {
  iconAnchor: [25, 7],
  raw,
  component
};
