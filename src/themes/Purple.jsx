import PropTypes from 'prop-types';
import _merge from 'lodash/merge';
import { colors } from 'constants/index';
import { buildDarkThemeConfig, themeFactory } from 'helpers/themes';
import { config as defaultThemeConfig } from './Default';

export const config = _merge(
  {},
  buildDarkThemeConfig(defaultThemeConfig, colors, 'purple'),
  {}
);

const PurpleTheme = themeFactory(config);

PurpleTheme.propTypes = {
  children: PropTypes.node.isRequired
};

PurpleTheme.displayName = 'PurpleTheme';

export default PurpleTheme;
