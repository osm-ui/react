import PropTypes from 'prop-types';
import _merge from 'lodash/merge';
import { colors } from 'constants/index';
import { buildLightThemeConfig, themeFactory } from 'helpers/themes';
import { config as defaultThemeConfig } from './Default';

export const config = _merge(
  {},
  buildLightThemeConfig(defaultThemeConfig, colors, 'yellow'),
  {}
);

const YellowTheme = themeFactory(config);

YellowTheme.propTypes = {
  children: PropTypes.node.isRequired
};

YellowTheme.displayName = 'YellowTheme';

export default YellowTheme;
