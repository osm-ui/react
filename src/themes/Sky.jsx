import PropTypes from 'prop-types';
import _merge from 'lodash/merge';
import { colors } from 'constants/index';
import { buildLightThemeConfig, themeFactory } from 'helpers/themes';
import { config as defaultThemeConfig } from './Default';

export const config = _merge(
  {},
  buildLightThemeConfig(defaultThemeConfig, colors, 'sky'),
  {}
);

const SkyTheme = themeFactory(config);

SkyTheme.propTypes = {
  children: PropTypes.node.isRequired
};

SkyTheme.displayName = 'SkyTheme';

export default SkyTheme;
