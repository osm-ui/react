import PropTypes from 'prop-types';
import _merge from 'lodash/merge';
import { colors } from 'constants/index';
import { buildDarkThemeConfig, themeFactory } from 'helpers/themes';
import { config as defaultThemeConfig } from './Default';

export const config = _merge(
  {},
  buildDarkThemeConfig(defaultThemeConfig, colors, 'rose'),
  {}
);

const RoseTheme = themeFactory(config);

RoseTheme.propTypes = {
  children: PropTypes.node.isRequired
};

RoseTheme.displayName = 'RoseTheme';

export default RoseTheme;
