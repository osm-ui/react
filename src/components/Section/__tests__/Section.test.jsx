import React from 'react';
import 'jest-styled-components';
import { shallow } from 'enzyme';
import Section from '..';

describe('When using snapshots', () => {
  it('Should render with no props', () => {
    const wrapper = shallow(<Section />);
    expect(wrapper).toMatchSnapshot();
  });

  it('Should render with correct classnames', () => {
    const wrapper = shallow(<Section dimmed appCanvas className='test' />);
    expect(wrapper).toMatchSnapshot();
  });
});
