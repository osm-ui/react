import React from 'react';
import 'jest-styled-components';
import { shallow } from 'enzyme';
import { snapshotWithElementChildren } from 'helpers/tests';
import List from '..';

describe('When using snapshots', () => {
  it('Should render with an element children', () =>
    snapshotWithElementChildren(List));
});

describe('When testing initialization', () => {
  it('Should render with correct classnames', () => {
    const wrapper = shallow(<List className='test' />);

    expect(wrapper.hasClass('test')).toBe(true);
  });

  it('Should render with correct props', () => {
    const wrapper = shallow(<List test='1' />);

    expect(wrapper.prop('test')).toBe('1');
  });
});
