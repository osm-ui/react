import React from 'react';

import { StyledItem } from './List.style';

const ListItem = props => <StyledItem {...props} />;

ListItem.propTypes = {};
ListItem.defaultProps = {};

ListItem.displayName = 'List.Item';
ListItem.style = StyledItem;

export default ListItem;
