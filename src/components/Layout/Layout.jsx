import React from 'react';

import {
  StyledTitle,
  StyledHeader,
  StyledFooter,
  StyledNav
} from './Layout.style';

export const Title = props => <StyledTitle {...props} />;

Title.propTypes = {};
Title.defaultProps = {};

Title.displayName = 'Title';
Title.style = StyledTitle;

export const Header = props => <StyledHeader {...props} />;

Header.propTypes = {};
Header.defaultProps = {};

Header.displayName = 'Header';
Header.style = StyledHeader;

export const Footer = props => <StyledFooter {...props} />;

Footer.propTypes = {};
Footer.defaultProps = {};

Footer.displayName = 'Footer';
Footer.style = StyledFooter;

export const Nav = props => <StyledNav {...props} />;

Nav.propTypes = {};
Nav.defaultProps = {};

Nav.displayName = 'Nav';
Nav.style = StyledNav;
