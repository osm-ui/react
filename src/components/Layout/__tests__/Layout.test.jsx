import React from 'react';
import 'jest-styled-components';
import { shallow } from 'enzyme';
import { snapshotWithElementChildren } from 'helpers/tests';
import { Title, Header, Footer, Nav, Scrollable } from '..';

jest.mock('helpers/dom');

[Title, Header, Footer, Nav, Scrollable].forEach(Component => {
  describe(`When using snapshots, ${Component.displayName}`, () => {
    it('should render with an element children', () =>
      snapshotWithElementChildren(Component));
  });

  describe(`When testing initialization, ${Component.displayName}`, () => {
    it('Should render with correct classnames', () => {
      const wrapper = shallow(
        <Component className='test'>This is a text</Component>
      );

      expect(wrapper.hasClass('test')).toBe(true);
    });

    it('Should render with correct props', () => {
      const wrapper = shallow(<Component test='1'>This is a text</Component>);

      expect(wrapper.prop('test')).toBe('1');
    });

    if (Component === Scrollable) {
      it('Should render with correct state', () => {
        const wrapper = shallow(<Component>This is a text</Component>);

        expect(wrapper.state('displayUp')).toBe(false);
        expect(wrapper.state('displayDown')).toBe(false);
      });
    }
  });
});
