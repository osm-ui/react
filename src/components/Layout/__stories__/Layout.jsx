import React from 'react';
import { storiesOf } from '@storybook/react';
import { host } from 'storybook-host';
import { withKnobs, select } from '@storybook/addon-knobs';
import Lorem from 'react-lorem-component';
import styled from 'styled-components';

import defaultHostOptions from 'helpers/__stories__/defaultHostOptions';

const StyledDiv = styled.div`
  position: relative;
  height: 15rem;
`;

import {
  DefaultTheme,
  WhiteTheme,
  LightGrayTheme,
  DarkGrayTheme,
  AnthraciteTheme,
  YellowTheme,
  OrangeTheme,
  BrownTheme,
  RedTheme,
  RoseTheme,
  PurpleTheme,
  BlueTheme,
  SkyTheme,
  TurquoiseTheme,
  GreenTheme,
  Title,
  Header,
  Footer,
  Nav,
  Scrollable
} from 'index';

import {} from 'index';

storiesOf('Layout', module)
  .addDecorator(
    host({
      ...defaultHostOptions,
      title: 'Layout'
    })
  )
  .addWithInfo('Title', () => (
    <DefaultTheme>
      <Title>This is a nice title</Title>
      <Lorem count={2} />
    </DefaultTheme>
  ))
  .addWithInfo('Header', () => (
    <DefaultTheme>
      <Header>
        <b>This is located inside a header</b>
      </Header>
      <Lorem count={2} />
    </DefaultTheme>
  ))
  .addWithInfo('Footer', () => (
    <DefaultTheme>
      <Lorem count={2} />
      <Footer>
        <b>This is located inside a footer</b>
      </Footer>
    </DefaultTheme>
  ))
  .addWithInfo('Nav', () => (
    <DefaultTheme>
      <Nav>
        <a>First</a>
        <a>Second</a>
        <a>Third</a>
      </Nav>
    </DefaultTheme>
  ))
  .addWithInfo('Scrollable', () => (
    <DefaultTheme>
      <StyledDiv>
        <Scrollable>
          <Lorem count={10} />
        </Scrollable>
      </StyledDiv>
    </DefaultTheme>
  ))
  .addDecorator(withKnobs)
  .addWithInfo('Playground', () => {
    const theme = select(
      'Theme',
      [
        'Default',
        'White',
        'LightGray',
        'DarkGray',
        'Anthracite',
        'Yellow',
        'Orange',
        'Brown',
        'Red',
        'Rose',
        'Purple',
        'Blue',
        'Sky',
        'Turquoise',
        'Green'
      ],
      'Red'
    );
    const themes = {
      DefaultTheme,
      WhiteTheme,
      LightGrayTheme,
      DarkGrayTheme,
      AnthraciteTheme,
      YellowTheme,
      OrangeTheme,
      BrownTheme,
      RedTheme,
      RoseTheme,
      PurpleTheme,
      BlueTheme,
      SkyTheme,
      TurquoiseTheme,
      GreenTheme
    };
    const ThemeElement = themes[`${theme}Theme`];

    const position = select('Scrollable Position', ['right', 'left'], 'left');

    return (
      <DefaultTheme>
        <ThemeElement>
          <Header>
            <Title>My Title inside a header</Title>
          </Header>
          <Nav>
            <a>First</a>
            <a>Second</a>
            <a>Third</a>
          </Nav>
          <StyledDiv>
            <Scrollable position={position}>
              <Lorem count={10} />
            </Scrollable>
          </StyledDiv>
          <Footer>
            <b>This is located inside a footer</b>
          </Footer>
        </ThemeElement>
      </DefaultTheme>
    );
  });
