import styled from 'styled-components';

import { getRawTheme } from 'helpers/themes';

export const StyledTitle = styled.h2.attrs({
  theme: getRawTheme
})`
  color: ${p => p.theme.color};
  background-color: ${p => p.theme.backgroundColor};

  margin: 0.5rem 0;
  line-height: 3rem;
`;

export const StyledHeader = styled.header.attrs({
  theme: getRawTheme
})`
  color: ${p => p.theme.color};
  background-color: ${p => p.theme.backgroundColor};

  padding: 1rem 0;

  ${StyledTitle} {
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
  }
`;

export const StyledFooter = styled.footer.attrs({
  theme: getRawTheme
})`
  color: ${p => p.theme.color};
  background-color: ${p => p.theme.backgroundColor};

  padding: 1rem 0;
`;

export const StyledNav = styled.nav.attrs({
  theme: getRawTheme
})`
  color: ${p => p.theme.color};
  background-color: ${p => p.theme.backgroundColor};
  padding: 0.5rem 0;

  a {
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    text-decoration: none;

    display: block;
    margin: 0.5rem 0;
    padding: 1rem 1.5rem;
    border-radius: ${p => p.theme.nav.borderRadius};
    background-color: ${p => p.theme.nav.backgroundColor};
    color: ${p => p.theme.nav.color};
    font-size: ${p => p.theme.nav.fontSize};
    line-height: ${p => p.theme.nav.lineHeight};
    font-weight: ${p => p.theme.nav.fontWeight};
  }

  a:hover,
  a:active,
  a:focus {
    text-decoration: none;
    color: ${p => p.theme.nav.hoverColor};
    background-color: ${p => p.theme.nav.hoverBackgroundColor};
  }
`;
