import styled from 'styled-components';

import { makeTransparent, getRawTheme } from 'helpers/themes';

const StyledScrollable = styled.div.attrs({
  theme: getRawTheme
})`
  position: relative;
  height: 100%;
  color: ${props => props.theme.color};
  background-color: ${props => props.theme.backgroundColor};

  .content {
    height: 100%;
    overflow: scroll;
  }

  &::before,
  &::after {
    position: absolute;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 1rem;
    height: 1rem;
    border-radius: 50%;
    background-color: ${props => makeTransparent(props.theme.backgroundColor)};
    left: ${props => (props.position === 'left' ? '3px' : 'unset')};
    right: ${props => (props.position === 'right' ? '3px' : 'unset')};
    font-family: 'Font Awesome 5 Free';
    font-size: 0.8rem;
    font-weight: 900;
    z-index: 1000;
  }

  &.up::before {
    content: '\f106';
    top: 1px;
  }

  &.down::after {
    content: '\f107';
    bottom: 1px;
  }
`;

export default StyledScrollable;
