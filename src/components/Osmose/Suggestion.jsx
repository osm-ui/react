import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { osmoseSuggestionTypes as types } from 'constants/index';

import StyledSuggestion from './Suggestion.style';

const titleObj = {
  [types.NEW]: 'New data',
  [types.ORIGINAL]: 'Data in OSM',
  [types.MODIFICATION]: 'Suggestion n°'
};

class Suggestion extends React.PureComponent {
  renderTag = (tag, type) => {
    return (
      <div className={classnames('info', type)} key={tag.k}>
        <div className='tag'>{tag.k}</div>
        <div className='value'>{tag.v}</div>
      </div>
    );
  };

  renderTags = tags => {
    if (!tags) return null;

    return <div className='infos'>{tags.map(tag => this.renderTag(tag))}</div>;
  };

  renderTagFixes = data => {
    return (
      <div className='infos'>
        {data.add.map(tag => this.renderTag(tag, 'add'))}
        {data.mod.map(tag => this.renderTag(tag, 'mod'))}
        {data.del.map(tag => this.renderTag(tag, 'del'))}
      </div>
    );
  };

  render() {
    const {
      osm,
      fixes,
      type,
      number,
      title,
      handleClick,
      className
    } = this.props;

    const suggestion =
      fixes !== null ? this.renderTagFixes(fixes) : this.renderTags(osm);

    const classNames = classnames(fixes ? 'fix' : 'osm', className);

    let finalTitle;

    if (title) finalTitle = title;
    else {
      const baseTitle = titleObj[type];
      finalTitle =
        type === types.MODIFICATION ? `${baseTitle}${number}` : baseTitle;
    }

    return (
      <StyledSuggestion className={classNames} onClick={handleClick}>
        <div className='title'>{finalTitle}</div>
        {suggestion}
      </StyledSuggestion>
    );
  }
}

Suggestion.propTypes = {
  type: PropTypes.oneOf(Object.keys(types)),
  number: PropTypes.number,
  osm: PropTypes.array,
  fixes: PropTypes.object,
  handleClick: PropTypes.func,
  title: PropTypes.string,
  className: PropTypes.string
};

Suggestion.defaultProps = {
  type: types.MODIFICATION,
  number: null,
  osm: null,
  fixes: null,
  handleClick: null,
  title: null,
  className: ''
};

Suggestion.displayName = 'Suggestion';
Suggestion.style = StyledSuggestion;

export default Suggestion;
