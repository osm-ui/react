import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { host } from 'storybook-host';
import { withKnobs } from '@storybook/addon-knobs';
import defaultHostOptions from 'helpers/__stories__/defaultHostOptions';

import { DefaultTheme, Osmose } from 'index';

import { fixData, addData } from 'helpers/__mocks__/osmose';

storiesOf('Osmose', module)
  .addDecorator(withKnobs)
  .addDecorator(
    host({
      ...defaultHostOptions,
      title: 'Osmose'
    })
  )
  .addWithInfo('Data to fix', () => (
    <DefaultTheme>
      <Osmose data={fixData} handleSuggestion={action('suggestion')} />
    </DefaultTheme>
  ))
  .addWithInfo('New data', () => (
    <DefaultTheme>
      <Osmose data={addData} handleSuggestion={action('suggestion')} />
    </DefaultTheme>
  ));
