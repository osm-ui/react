import React from 'react';
import 'jest-styled-components';
import { snapshotWithElementChildren } from 'helpers/tests';
import { shallow } from 'enzyme';
import Osmose from '..';

import { formatDeletedTags } from 'helpers/osmose';
import { addData, fixData } from 'helpers/__mocks__/osmose';
import { osmoseSuggestionTypes as types } from 'constants/index';

describe('When using snapshots', () => {
  it('Should render with an element children', () =>
    snapshotWithElementChildren(Osmose.Suggestion));

  it('Should render with osm fixable data', () => {
    const wrapper = shallow(
      <Osmose.Suggestion
        fixes={formatDeletedTags(
          fixData.elems[0].fixes[1],
          fixData.elems[0].tags
        )}
        number={1}
      />
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('Should render with custom title', () => {
    const wrapper = shallow(<Osmose.Suggestion title='test' />);
    expect(wrapper).toMatchSnapshot();
  });

  it('Should render with only osm', () => {
    const wrapper = shallow(<Osmose.Suggestion osm={fixData.elems[0].tags} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('Should render with new data', () => {
    const wrapper = shallow(
      <Osmose.Suggestion fixes={addData.new_elems[0]} type={types.NEW} />
    );
    expect(wrapper).toMatchSnapshot();
  });
});

describe('When testing interactions', () => {
  it('Should call callback when clicked', () => {
    const callback = jest.fn();
    const wrapper = shallow(<Osmose.Suggestion handleClick={callback} />);

    wrapper.simulate('click');
    expect(callback).toHaveBeenCalledTimes(1);
  });
});
