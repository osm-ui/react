import React from 'react';
import 'jest-styled-components';
import { shallow, mount } from 'enzyme';
import Osmose from '..';

import { fixData, addData } from 'helpers/__mocks__/osmose';

describe('When using snapshots', () => {
  it('Should render with no data', () => {
    const emptyWrapper = shallow(
      <Osmose data={{}} handleSuggestion={jest.fn()} />
    );
    expect(emptyWrapper).toMatchSnapshot();

    const noElemWrapper = shallow(
      <Osmose
        data={{
          elems: [],
          new_elems: []
        }}
        handleSuggestion={jest.fn()}
      />
    );
    expect(noElemWrapper).toMatchSnapshot();
  });

  it('Should render with data', () => {
    const wrapperWithNewData = shallow(
      <Osmose data={addData} handleSuggestion={jest.fn()} />
    );
    expect(wrapperWithNewData).toMatchSnapshot();

    const wrapperWithFixData = shallow(
      <Osmose data={fixData} handleSuggestion={jest.fn()} />
    );
    expect(wrapperWithFixData).toMatchSnapshot();
  });
});

describe('When testing initialization', () => {
  it('Should render with correct classnames', () => {
    const wrapper = shallow(
      <Osmose className='test' data={{}} handleSuggestion={jest.fn()} />
    );
    expect(wrapper.hasClass('test')).toEqual(true);
  });
});

describe('When testing callbacks', () => {
  it('Should call the handleSuggestion callback when a suggestion is clicked', () => {
    const callback = jest.fn();

    const wrapper = mount(
      <Osmose data={fixData} handleSuggestion={callback} />
    );

    const suggestion1 = wrapper.find(Osmose.Suggestion).first();
    const suggestion2 = wrapper.find(Osmose.Suggestion).at(1);
    const suggestion3 = wrapper.find(Osmose.Suggestion).at(2);

    suggestion1.simulate('click');
    suggestion2.simulate('click');
    suggestion3.simulate('click');

    const newDataWrapper = mount(
      <Osmose data={addData} handleSuggestion={callback} />
    );

    const suggestion4 = newDataWrapper.find(Osmose.Suggestion).first();
    suggestion4.simulate('click');

    expect(callback).toHaveBeenCalledTimes(4);
  });
});
