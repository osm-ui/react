import React from 'react';
import 'jest-styled-components';
import { shallow, mount } from 'enzyme';
import { snapshotWithoutChildren } from 'helpers/tests';

import IconPicker from '..';

describe('When using snapshots', () => {
  it('Should render with an element children', () => {
    snapshotWithoutChildren(IconPicker, { onChoose: jest.fn() });
  });
});

describe('When testing initialization', () => {
  it('Should render with correct classnames', () => {
    const wrapper = shallow(
      <IconPicker className='test' onChoose={jest.fn()} />
    );

    expect(wrapper.hasClass('test')).toBe(true);
  });

  it('Should set correct category when given an icon prop', () => {
    const wrapper = shallow(
      <IconPicker selectedIcon='crow' onChoose={jest.fn()} />
    );

    expect(wrapper.state('filter')).toBe('animals');
  });
});

describe('When testing interactions', () => {
  it('Should update filter state when clicked on a category', () => {
    const wrapper = mount(<IconPicker onChoose={jest.fn()} />);
    const initialFilter = wrapper.state('filter');

    wrapper
      .find('.category')
      .at(1)
      .simulate('click');

    expect(wrapper.state('filter')).not.toBe(initialFilter);
  });

  it('Should toggle categories labels when clicked on header or category label', () => {
    const wrapper = mount(<IconPicker onChoose={jest.fn()} />);

    const header = wrapper.find('.header');
    const getCategories = () => wrapper.find('.categories').first();

    header.simulate('click');

    expect(wrapper.state('categoriesOpened')).toBe(true);
    expect(getCategories().hasClass('opened')).toBe(true);

    header.simulate('click');

    expect(wrapper.state('categoriesOpened')).toBe(false);
    expect(getCategories().hasClass('opened')).toBe(false);

    header.simulate('click');
    header.simulate('click');

    expect(wrapper.state('categoriesOpened')).toBe(false);
    expect(getCategories().hasClass('opened')).toBe(false);
  });

  it('Should select an icon when clicked', () => {
    const wrapper = mount(<IconPicker onChoose={jest.fn()} />);

    const getIcon = () => wrapper.find('.icons .content').childAt(0);
    getIcon().simulate('click');

    expect(wrapper.state('selectedIcon')).toBe('accessible-icon');
    expect(getIcon().hasClass('selected')).toBe(true);
  });
});
