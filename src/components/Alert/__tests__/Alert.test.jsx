import React from 'react';
import 'jest-styled-components';
import { shallow } from 'enzyme';
import { snapshotWithElementChildren } from 'helpers/tests';
import Alert from '..';

describe('When using snapshots', () => {
  it('Should render with an element children', () =>
    snapshotWithElementChildren(Alert));
});

describe('When testing initialization', () => {
  it('Should render with correct default classnames', () => {
    const wrapper = shallow(<Alert />);

    expect(wrapper.hasClass('alert')).toBe(true);
    expect(wrapper.hasClass('alert-info')).toBe(true);
  });

  it('Should render with correct classnames', () => {
    const wrapper = shallow(<Alert className='test' context='success' />);

    expect(wrapper.hasClass('test')).toBe(true);
    expect(wrapper.hasClass('alert-success')).toBe(true);
  });
});
