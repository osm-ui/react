import styled from 'styled-components';

import { getRawTheme } from 'helpers/themes';

export const contexts = ['info', 'success', 'warning', 'danger'];

const colorsStyle = props =>
  contexts.reduce((reducedStyles, context) => {
    const colors = props.theme.alert[context];

    return `
      ${reducedStyles}

      &.alert-${context} {
          color: ${colors.color};
          background-color: ${colors.backgroundColor};

          a {
              color: ${colors.color};
              text-decoration: underline;
          }

          a:hover, a:focus, a:active {
              color: ${colors.color};
              text-decoration: none;
          }
      }
    `;
  }, '');

const StyledAlert = styled.div.attrs({
  theme: getRawTheme
})`
  padding-top: ${props => props.theme.alert.verticalPadding};
  padding-bottom: ${props => props.theme.alert.verticalPadding};
  padding-left: ${props => props.theme.alert.horizontalPadding};
  padding-right: ${props => props.theme.alert.horizontalPadding};
  border-radius: ${props => props.theme.alert.borderRadius};

  ${props => colorsStyle(props)};
`;

export default StyledAlert;
