import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';

import shapes from 'assets/markers';
import { Wrapper } from './OffMapMarker.style';

export const OffMapMarker = ({
  theme,
  color,
  shape,
  icon,
  // iconType,
  ...props
}) => {
  const wrapperClasses = classNames({
    'osm-ui-react-marker-shape': true,
    [`osm-ui-react-marker-${shape}`]: true,
    [`osm-ui-react-marker-${theme}`]: true
  });
  const Svg = shapes[shape].component;

  return (
    <Wrapper className={wrapperClasses} color={color} {...props}>
      <Svg />
      <div className='osm-ui-react-marker-icon-wrapper'>
        <i className={`fa fa-${icon}`} />
      </div>
    </Wrapper>
  );
};

const shapeNames = Object.keys(shapes);

OffMapMarker.propTypes = {
  theme: PropTypes.string,
  color: PropTypes.string,
  shape: PropTypes.oneOf(shapeNames),
  icon: PropTypes.string
  // iconType: PropTypes.oneOf(['font-awesome']),
};

OffMapMarker.defaultProps = {
  theme: 'default',
  color: '',
  shape: shapeNames[0],
  icon: ''
  // iconType: 'font-awesome',
};

OffMapMarker.displayName = 'OffMapMarker';

export default OffMapMarker;
