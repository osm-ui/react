import styled from 'styled-components';

import { buildMarkerIconColor } from 'helpers/themes';

export const Wrapper = styled.div`
  &.osm-ui-react-marker-shape {
    position: relative;
    display: inline-flex;
    align-items: center;
    justify-content: center;
    width: 4rem !important;
    height: 4rem !important;
    cursor: default;

    svg,
    .osm-ui-react-marker-icon-wrapper {
      cursor: default;
    }

    svg {
      position: static;

      #colorized,
      #colorized * {
        ${p => (p.color ? `fill: ${p.color} !important;` : '')};
      }
    }

    .osm-ui-react-marker-icon-wrapper {
      position: absolute;
      width: 100%;
      height: 100%;
      display: flex;
      align-items: center;
      justify-content: center;

      i {
        transform: translateY(-3px);
        ${p =>
          p.color ? `color: ${buildMarkerIconColor(p.color)} !important;` : ''};
      }
    }

    &.osm-ui-react-marker-pointerClassic,
    &.osm-ui-react-marker-pointerClassicThin,
    &.osm-ui-react-marker-pointerCirclePin {
      .osm-ui-react-marker-icon-wrapper {
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
      }
    }
  }
`;
