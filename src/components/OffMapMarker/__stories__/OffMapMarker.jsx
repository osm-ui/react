import React from 'react';
import { storiesOf } from '@storybook/react';
import { host } from 'storybook-host';
import { withKnobs, select, text } from '@storybook/addon-knobs';

import shapes from 'assets/markers';
import defaultHostOptions from 'helpers/__stories__/defaultHostOptions';
import KnobsAlert from 'helpers/__stories__/components/KnobsAlert';

import { DefaultTheme, OffMapMarker } from 'index';

storiesOf('OffMapMarker', module)
  .addDecorator(withKnobs)
  .addDecorator(
    host({
      ...defaultHostOptions,
      title: 'OffMapMarker'
    })
  )
  .addWithInfo('Default state', () => (
    <DefaultTheme>
      <section>
        <OffMapMarker theme='red' shape='pointerCirclePin' icon='fire' />
        <OffMapMarker theme='blue' shape='pointerClassic' icon='tint' />
        <OffMapMarker theme='green' shape='pointerClassicThin' icon='tree' />
        <OffMapMarker theme='anthracite' shape='pointerClassicThin' />
        <OffMapMarker theme='darkGray' shape='pointerClassic' />
        <OffMapMarker theme='lightGray' shape='pointerCirclePin' />
      </section>
      <section>
        <OffMapMarker color='#84e6c0' icon='bolt' shape='pointerCirclePin' />
        <OffMapMarker
          color='rgb(242, 188, 224)'
          icon='sun'
          shape='pointerClassic'
        />
        <OffMapMarker
          color='hsl(223, 57%, 80%)'
          icon='glasses'
          shape='pointerClassicThin'
        />
      </section>
      <section>
        <OffMapMarker theme='rose' shape='basicCircle' />
        <OffMapMarker theme='yellow' shape='basicDiamond' />
        <OffMapMarker theme='sky' shape='basicSquare' />
        <OffMapMarker theme='brown' shape='basicDownTriangle' />
        <OffMapMarker theme='turquoise' shape='basicLeftTriangle' />
        <OffMapMarker theme='white' shape='basicUpTriangle' />
        <OffMapMarker theme='orange' shape='basicRightTriangle' />
      </section>
    </DefaultTheme>
  ))
  .addWithInfo('Playground', () => {
    const shape = select('Shape', Object.keys(shapes), 'pointerCirclePin');
    const icon = text('Icon (Font Awesome icon name)', 'hand-spock');

    const theme = select(
      'Theme',
      [
        'Default',
        'White',
        'LightGray',
        'DarkGray',
        'Anthracite',
        'Yellow',
        'Orange',
        'Brown',
        'Red',
        'Rose',
        'Purple',
        'Blue',
        'Sky',
        'Turquoise',
        'Green'
      ],
      'Rose'
    );

    return (
      <DefaultTheme>
        <KnobsAlert />
        <OffMapMarker theme={theme.toLowerCase()} shape={shape} icon={icon} />
      </DefaultTheme>
    );
  });
