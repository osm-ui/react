import React from 'react';
import 'jest-styled-components';
import { shallow } from 'enzyme';
import Toolbar from '..';

describe('When using snapshots', () => {
  it('Should render correctly', () => {
    const wrapper = shallow(
      <Toolbar.Item icon='bars'>
        <div />
      </Toolbar.Item>
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('Should render correctly with non default props', () => {
    const wrapper = shallow(
      <Toolbar.Item
        className='test'
        icon='bars'
        loading
        size='xs'
        type='anchor'
        shape='square'
        inGroup
        inactive
      />
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('Should render correctly with anchor type', () => {
    const wrapper = shallow(<Toolbar.Item type='anchor' />);

    expect(wrapper).toMatchSnapshot();
  });
});
