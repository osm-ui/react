import React from 'react';
import 'jest-styled-components';
import { shallow } from 'enzyme';
import Toolbar from '..';
import StyledGroup from '../styles/Group.style';

describe('When using initialization', () => {
  it('Should render correctly', () => {
    const wrapper = shallow(
      <Toolbar.Group>
        <Toolbar.Item icon='bars' />
      </Toolbar.Group>
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('Should render with correct default classnames', () => {
    const wrapper = shallow(
      <Toolbar.Group>
        <Toolbar.Item icon='bars' />
      </Toolbar.Group>
    );

    expect(wrapper.find(StyledGroup).hasClass('direction-column')).toBe(true);
  });

  it('Should render with the correct classnames', () => {
    const wrapper = shallow(
      <Toolbar.Group className='test' direction='row' size='xs' shape='square'>
        <Toolbar.Item icon='bars' />
      </Toolbar.Group>
    );

    const group = wrapper.find(StyledGroup);

    expect(group.hasClass('test')).toBe(true);
    expect(group.hasClass('direction-row')).toBe(true);
    expect(group.hasClass('first-size-xs')).toBe(true);
    expect(group.hasClass('first-shape-square')).toBe(true);
    expect(group.hasClass('last-size-xs')).toBe(true);
    expect(group.hasClass('last-shape-square')).toBe(true);
  });

  it('Should render with the correct classnames depending on children', () => {
    const wrapper = shallow(
      <Toolbar.Group>
        <Toolbar.Item icon='bars' size='xs' shape='square' />
        <Toolbar.Item icon='bars' size='md' shape='round' />
      </Toolbar.Group>
    );

    const group = wrapper.find(StyledGroup);

    expect(group.hasClass('first-size-xs')).toBe(true);
    expect(group.hasClass('first-shape-square')).toBe(true);
    expect(group.hasClass('last-size-md')).toBe(true);
    expect(group.hasClass('last-shape-round')).toBe(true);
  });

  it('Shoulde render its children with correct props', () => {
    const wrapper = shallow(
      <Toolbar.Group size='xs' shape='square'>
        <Toolbar.Item icon='bars' />
      </Toolbar.Group>
    );

    const item = wrapper.find(Toolbar.Item);

    expect(item.prop('inGroup')).toBe(true);
    expect(item.prop('size')).toBe('xs');
    expect(item.prop('shape')).toBe('square');
  });
});
