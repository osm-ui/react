import React from 'react';
import 'jest-styled-components';
import { shallow } from 'enzyme';
import Toolbar from '..';

describe('When using snapshots', () => {
  it('Should render correctly', () => {
    const wrapper = shallow(
      <Toolbar.Collapse icon='bars'>
        <div />
      </Toolbar.Collapse>
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('Should render correctly with non default props', () => {
    const wrapper = shallow(
      <Toolbar.Collapse
        className='test'
        icon='bars'
        size='xs'
        type='anchor'
        position='center-right'
        direction='row'
        shape='square'
      >
        <div />
      </Toolbar.Collapse>
    );

    expect(wrapper).toMatchSnapshot();
  });
});

describe('When testing the callbacks', () => {
  it('Should call the callbacks when the opened prop is passed from the beginning', () => {
    const onOpen = jest.fn();
    const onClose = jest.fn();
    const collapse = shallow(
      <Toolbar.Collapse opened onOpen={onOpen} onClose={onClose}>
        <div />
      </Toolbar.Collapse>
    );

    expect(onOpen).toBeCalledTimes(1);
    expect(onClose).not.toBeCalled();

    collapse.setProps({ opened: false });

    expect(onOpen).toBeCalledTimes(1);
    expect(onClose).toBeCalledTimes(1);

    collapse.setProps({ opened: true });

    expect(onOpen).toBeCalledTimes(2);
    expect(onClose).toBeCalledTimes(1);

    collapse.setProps({ opened: false });

    expect(onOpen).toBeCalledTimes(2);
    expect(onClose).toBeCalledTimes(2);
  });

  it('Should close and call the onClick callback when clicked with opened prop', () => {
    const onClick = jest.fn();
    const collapse = shallow(
      <Toolbar.Collapse opened onClick={onClick}>
        <div />
      </Toolbar.Collapse>
    );

    collapse.find(Toolbar.Item).simulate('click');
    expect(onClick).toBeCalledTimes(1);
    expect(collapse.state('opened')).toBe(false);
  });

  it('Should open and call the onClick callback when clicked without opened prop', () => {
    const onClick = jest.fn();
    const collapse = shallow(
      <Toolbar.Collapse onClick={onClick}>
        <div />
      </Toolbar.Collapse>
    );

    collapse.find(Toolbar.Item).simulate('click');
    expect(onClick).toBeCalledTimes(1);
    expect(collapse.state('opened')).toBe(true);
  });
});
