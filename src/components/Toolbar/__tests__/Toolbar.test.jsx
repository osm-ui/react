import React from 'react';
import 'jest-styled-components';
import { shallow, mount } from 'enzyme';
import Toolbar from '..';
import StyledToolbar from '../styles/Toolbar.style';

describe('When using initialization', () => {
  it('Should render correctly', () => {
    const wrapper = mount(
      <Toolbar>
        <div />
      </Toolbar>
    );

    const toolbar = wrapper.find(StyledToolbar);

    expect(toolbar.hasClass('opened')).toBe(false);
    expect(toolbar.hasClass('position-top-left')).toBe(true);
    expect(toolbar.hasClass('direction-column')).toBe(true);
    expect(toolbar.hasClass('container-parent')).toBe(true);
  });

  it('Should render with the correct classnames', () => {
    const wrapper = mount(
      <Toolbar
        className='test'
        opened
        position='bottom-right'
        direction='row'
        container='root'
      >
        <div />
      </Toolbar>
    );

    const toolbar = wrapper.find(StyledToolbar);

    expect(toolbar.hasClass('test')).toBe(true);
    expect(toolbar.hasClass('opened')).toBe(true);
    expect(toolbar.hasClass('position-bottom-right')).toBe(true);
    expect(toolbar.hasClass('direction-row')).toBe(true);
    expect(toolbar.hasClass('container-root')).toBe(true);
  });

  it('Shoulde render its children with correct props', () => {
    const wrapper = mount(
      <Toolbar size='xs' shape='square'>
        <Toolbar.Item icon='bars' />
      </Toolbar>
    );

    const item = wrapper.find(Toolbar.Item);

    expect(item.prop('size')).toBe('xs');
    expect(item.prop('shape')).toBe('square');
  });
});

describe('When testing the callbacks', () => {
  it('Should call the callbacks when the opened prop is passed from the beginning', () => {
    const onOpen = jest.fn();
    const onClose = jest.fn();
    const toolbar = shallow(
      <Toolbar opened onOpen={onOpen} onClose={onClose}>
        <div />
      </Toolbar>
    );

    toolbar.setProps({ opened: false });
    toolbar.setProps({ opened: true });
    toolbar.setProps({ opened: false });

    expect(onOpen.mock.calls.length).toBe(2);
    expect(onClose.mock.calls.length).toBe(2);
  });

  it('Should call the callbacks when the opened prop is not passed from the beginning', () => {
    const onOpen = jest.fn();
    const onClose = jest.fn();
    const toolbar = shallow(
      <Toolbar opened={false} onOpen={onOpen} onClose={onClose}>
        <div />
      </Toolbar>
    );

    toolbar.setProps({ opened: true });
    toolbar.setProps({ opened: false });
    toolbar.setProps({ opened: true });

    expect(onClose.mock.calls.length).toBe(1);
    expect(onOpen.mock.calls.length).toBe(2);
  });
});
