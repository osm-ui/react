import styled from 'styled-components';

import { getRawTheme } from 'helpers/themes';

const StyledGroup = styled.div.attrs({
  theme: getRawTheme
})`
  display: flex;
  background: ${p => p.theme.toolbar.button.backgroundColor};
  box-shadow: ${p => p.theme.toolbar.boxShadow};

  &.direction-row {
    flex-direction: row;
  }
  &.direction-column {
    flex-direction: column;
  }

  &.direction-column {
    &.first-shape-square {
      border-top-left-radius: ${p => p.theme.toolbar.button.borderRadius};
      border-top-right-radius: ${p => p.theme.toolbar.button.borderRadius};
    }

    &.last-shape-square {
      border-bottom-left-radius: ${p => p.theme.toolbar.button.borderRadius};
      border-bottom-right-radius: ${p => p.theme.toolbar.button.borderRadius};
    }

    &.first-shape-round {
      border-top-left-radius: ${p => p.theme.toolbar[`${p.firstSize}Size`]};
      border-top-right-radius: ${p => p.theme.toolbar[`${p.firstSize}Size`]};
    }

    &.last-shape-round {
      border-bottom-left-radius: ${p => p.theme.toolbar[`${p.lastSize}Size`]};
      border-bottom-right-radius: ${p => p.theme.toolbar[`${p.lastSize}Size`]};
    }
  }

  &.direction-row {
    &.first-shape-square {
      border-top-left-radius: ${p => p.theme.toolbar.button.borderRadius};
      border-bottom-left-radius: ${p => p.theme.toolbar.button.borderRadius};
    }

    &.last-shape-square {
      border-top-right-radius: ${p => p.theme.toolbar.button.borderRadius};
      border-bottom-right-radius: ${p => p.theme.toolbar.button.borderRadius};
    }

    &.first-shape-round {
      border-top-left-radius: ${p => p.theme.toolbar[`${p.firstSize}Size`]};
      border-bottom-left-radius: ${p => p.theme.toolbar[`${p.firstSize}Size`]};
    }

    &.last-shape-round {
      border-top-right-radius: ${p => p.theme.toolbar[`${p.lastSize}Size`]};
      border-bottom-right-radius: ${p => p.theme.toolbar[`${p.lastSize}Size`]};
    }
  }
`;

export default StyledGroup;
