import styled from 'styled-components';

import { getRawTheme } from 'helpers/themes';

import Loader from 'components/Loader';

const StyledItem = styled.button.attrs({
  theme: getRawTheme
})`
  &,
  &.btn {
    position: relative;
    transition: all 0.1s ease-out;

    pointer-events: all;
    cursor: default;
    color: ${p => p.theme.toolbar.button.color};
    border-color: ${p => p.theme.toolbar.button.backgroundColor};
    border-style: ${p => p.theme.toolbar.button.borderStyle};
    border-width: ${p => p.theme.toolbar.button.borderWidth};
    background: ${p => p.theme.toolbar.button.backgroundColor};
    font-weight: 500;
    padding: 0;
    box-shadow: ${p => p.theme.toolbar.boxShadow};
  }

  &.shape-square {
    border-radius: ${p => p.theme.toolbar.button.borderRadius};
  }

  &.shape-round {
    border-radius: 50%;
  }

  &.xs {
    width: ${p => p.theme.toolbar.xsSize};
    height: ${p => p.theme.toolbar.xsSize};
    line-height: ${p => p.theme.toolbar.xsLineHeight};
    font-size: ${p => p.theme.toolbar.xsFontSize};

    .fas {
      font-size: ${p => p.theme.toolbar.xsIconSize};
    }
  }

  &.sm {
    width: ${p => p.theme.toolbar.smSize};
    height: ${p => p.theme.toolbar.smSize};
    line-height: ${p => p.theme.toolbar.smLineHeight};
    font-size: ${p => p.theme.toolbar.smFontSize};

    .fas {
      font-size: ${p => p.theme.toolbar.smIconSize};
    }
  }

  &.md {
    width: ${p => p.theme.toolbar.mdSize};
    height: ${p => p.theme.toolbar.mdSize};
    line-height: ${p => p.theme.toolbar.mdLineHeight};
    font-size: ${p => p.theme.toolbar.mdFontSize};

    .fas {
      font-size: ${p => p.theme.toolbar.mdIconSize};
    }
  }

  &.lg {
    width: ${p => p.theme.toolbar.lgSize};
    height: ${p => p.theme.toolbar.lgSize};
    line-height: ${p => p.theme.toolbar.lgLineHeight};
    font-size: ${p => p.theme.toolbar.lgFontSize};

    .fas {
      font-size: ${p => p.theme.toolbar.lgIconSize};
    }
  }

  &.in-group {
    box-shadow: none;
  }

  &.btn:hover {
    color: ${p => p.theme.toolbar.button.color};
    background-color: ${p => p.theme.toolbar.button.hoverBackgroundColor};
    border-color: ${p => p.theme.toolbar.button.hoverBorderColor};
  }

  &.btn:focus {
    color: ${p => p.theme.toolbar.button.color};
    background-color: ${p => p.theme.toolbar.button.focusBackgroundColor};
    border-color: ${p => p.theme.toolbar.button.focusBorderColor};
    outline: none;
  }

  &.btn:active {
    color: ${p => p.theme.toolbar.button.color};
    background-color: ${p => p.theme.toolbar.button.activeBackgroundColor};
    border-color: ${p => p.theme.toolbar.button.activeBorderColor};
  }

  ${Loader.style} {
    background-color: transparent;
  }
`;

export const StyledButton = styled(StyledItem)`
  pointer-events: none;
`;

export const StyledAnchor = styled(StyledItem)`
  font-weight: 500;
`;

export default StyledItem;
