import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import StyledGroup from './styles/Group.style';

const Group = ({ direction, size, shape, className, children, ...rest }) => {
  let firstShape = shape;
  let lastShape = shape;
  let firstSize = size;
  let lastSize = size;

  if (!size && children.length > 0) {
    firstSize = children[0].props.size;
    lastSize = children[children.length - 1].props.size;
  }

  if (!shape && children.length > 0) {
    firstShape = children[0].props.shape;
    lastShape = children[children.length - 1].props.shape;
  }

  const classes = classnames(className, {
    [`direction-${direction}`]: true,
    [`first-shape-${firstShape}`]: true,
    [`last-shape-${lastShape}`]: true,
    [`first-size-${firstSize}`]: true,
    [`last-size-${lastSize}`]: true
  });

  const childrenProps = {
    inGroup: true
  };

  if (size) {
    childrenProps.size = size;
  }

  if (shape) {
    childrenProps.shape = shape;
  }

  return (
    <StyledGroup
      className={classes}
      firstSize={firstSize}
      lastSize={lastSize}
      {...rest}
    >
      {React.Children.map(children, child =>
        React.cloneElement(child, childrenProps)
      )}
    </StyledGroup>
  );
};

Group.propTypes = {
  direction: PropTypes.string,
  size: PropTypes.oneOf(['', 'xs', 'sm', 'md', 'lg']),
  shape: PropTypes.oneOf(['', 'round', 'square']),
  className: PropTypes.string,
  children: PropTypes.node.isRequired
};

Group.defaultProps = {
  direction: 'column',
  size: '',
  shape: '',
  className: ''
};

Group.displayName = 'Toolbar.Group';
Group.style = StyledGroup;

export default Group;
