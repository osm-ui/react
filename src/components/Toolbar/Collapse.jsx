import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import ToolbarItem from './Item';

import StyledCollapse, { Wrapper } from './styles/Collapse.style';

class Collapse extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      opened: props.opened
    };
  }

  componentDidMount() {
    if (this.props.opened === true) {
      this._triggerCallback('onOpen');
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      opened: nextProps.opened
    });

    if (this.props.opened !== nextProps.opened) {
      if (nextProps.opened === true) {
        this._triggerCallback('onOpen');
      } else {
        this._triggerCallback('onClose');
      }
    }
  }

  _triggerCallback = name => {
    if (this.props[name] !== null) {
      this.props[name]();
    }
  };

  _handleClick = () => {
    const newState = !this.state.opened;

    this.setState({
      opened: newState
    });

    this._triggerCallback('onClick');

    if (newState === true) {
      this._triggerCallback('onOpen');
    } else {
      this._triggerCallback('onClose');
    }
  };

  render() {
    const {
      position,
      direction,
      icon,
      size,
      shape,
      className,
      children,
      ...rest
    } = this.props;

    const classes = classnames(className, {
      [`position-${position}`]: true,
      [`direction-${direction}`]: true,
      opened: this.state.opened
    });

    const childrenProps = {
      direction
    };

    if (size) {
      childrenProps.size = size;
    }

    if (shape) {
      childrenProps.shape = shape;
    }

    const elements = [
      <ToolbarItem
        key='button'
        icon={icon}
        size={childrenProps.size}
        onClick={e => this._handleClick(e)}
      />,
      <StyledCollapse key='collapse' className={classes} {...rest}>
        {React.Children.map(children, child =>
          React.cloneElement(child, childrenProps)
        )}
      </StyledCollapse>
    ];

    if (
      (direction === 'column' &&
        ['bottom-left', 'bottom-center', 'bottom-right'].indexOf(position) >
          -1) ||
      (direction === 'row' &&
        ['top-right', 'center-right', 'bottom-right'].indexOf(position) > -1)
    ) {
      elements.reverse();
    }

    return <Wrapper className={classes}>{elements}</Wrapper>;
  }
}

Collapse.propTypes = {
  position: PropTypes.oneOf([
    'top-left',
    'top-center',
    'top-right',
    'center-right',
    'bottom-right',
    'bottom-center',
    'bottom-left',
    'center-left'
  ]),
  direction: PropTypes.oneOf(['row', 'column']),
  icon: PropTypes.string,
  size: PropTypes.oneOf(['', 'xs', 'sm', 'md', 'lg']),
  shape: PropTypes.oneOf(['', 'round', 'square']),
  opened: PropTypes.bool,
  onClick: PropTypes.func,
  onOpen: PropTypes.func,
  onClose: PropTypes.func,
  className: PropTypes.string,
  children: PropTypes.node.isRequired
};

Collapse.defaultProps = {
  position: 'top-left',
  direction: 'column',
  icon: 'bars',
  size: '',
  shape: '',
  opened: false,
  onClick: null,
  onOpen: null,
  onClose: null,
  className: ''
};

Collapse.displayName = 'Toolbar.Collapse';
Collapse.style = StyledCollapse;

export default Collapse;
