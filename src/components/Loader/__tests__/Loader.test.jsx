import React from 'react';
import 'jest-styled-components';
import { shallow, mount } from 'enzyme';
import { snapshotWithElementChildren } from 'helpers/tests';
import Loader from '..';

describe('When using snapshots', () => {
  it('Should render with an element children', () =>
    snapshotWithElementChildren(Loader));
});

describe('When testing initialization', () => {
  it('Should render with correct classnames', () => {
    const wrapper = shallow(<Loader className='test' />);

    expect(wrapper.hasClass('test')).toBe(true);
  });

  it('Should render with centered classname if passed centered prop', () => {
    const wrapper = shallow(<Loader centered />);

    expect(wrapper.hasClass('centered')).toBe(true);
  });

  it('Should render with correct props', () => {
    const wrapper = shallow(<Loader test='1' />);

    expect(wrapper.prop('test')).toBe('1');
  });

  it('Should render with a label', () => {
    const wrapper = mount(<Loader label='test' />);

    expect(wrapper.text()).toBe('test');
  });
});
