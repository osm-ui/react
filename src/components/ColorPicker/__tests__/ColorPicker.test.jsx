import React from 'react';
import 'jest-styled-components';
import { shallow } from 'enzyme';
import { snapshotWithoutChildren } from 'helpers/tests';
import ColorPicker from '..';

describe('When using snapshots', () => {
  it('Should render with an element children', () =>
    snapshotWithoutChildren(ColorPicker, { onChoose: jest.fn() }));
});

describe('When testing classnames', () => {
  it('Should render with correct classnames', () => {
    const wrapper = shallow(
      <ColorPicker className='test' onChoose={jest.fn()} />
    );

    expect(wrapper.hasClass('test')).toBe(true);
  });
});
