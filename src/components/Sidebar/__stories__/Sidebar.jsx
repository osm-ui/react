import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { host } from 'storybook-host';
import {
  withKnobs,
  text,
  number,
  boolean,
  select
} from '@storybook/addon-knobs';
import Lorem from 'react-lorem-component';

import defaultHostOptions from 'helpers/__stories__/defaultHostOptions';
import KnobsAlert from 'helpers/__stories__/components/KnobsAlert';
import FakeApp from 'helpers/__stories__/components/FakeApp';
import FakeSidebarApp, {
  FakeSidebarCenter
} from 'helpers/__stories__/components/FakeSidebarApp';

import {
  DefaultTheme,
  WhiteTheme,
  LightGrayTheme,
  DarkGrayTheme,
  AnthraciteTheme,
  YellowTheme,
  OrangeTheme,
  BrownTheme,
  RedTheme,
  RoseTheme,
  PurpleTheme,
  BlueTheme,
  SkyTheme,
  TurquoiseTheme,
  GreenTheme,
  Sidebar,
  Button
} from 'index';

storiesOf('Sidebar', module)
  .addDecorator(withKnobs)
  .addDecorator(
    host({
      ...defaultHostOptions,
      title: 'Sidebar'
    })
  )
  .addWithInfo('Default state', () => (
    <FakeSidebarApp>
      <Sidebar>
        <Lorem count={2} />
      </Sidebar>
    </FakeSidebarApp>
  ))
  .addWithInfo('With title', () => (
    <FakeSidebarApp>
      <Sidebar title='A sidebar title'>
        <Lorem count={2} />
      </Sidebar>
    </FakeSidebarApp>
  ))
  .addWithInfo('With back button', () => (
    <FakeSidebarApp>
      <Sidebar onClickBack={action('onClickBack')}>
        <Lorem count={2} />
      </Sidebar>
    </FakeSidebarApp>
  ))
  .addWithInfo('Header and footer', () => (
    <FakeSidebarApp>
      <Sidebar
        title='A sidebar title'
        header={<h3 style={{ margin: 0 }}>A header content</h3>}
        footer={
          <Button context='primary' block>
            A footer button
          </Button>
        }
      >
        <Lorem />
      </Sidebar>
    </FakeSidebarApp>
  ))
  .addWithInfo('Loading', () => (
    <FakeSidebarApp>
      <Sidebar title='A sidebar title' loading>
        <Lorem count={2} />
      </Sidebar>
    </FakeSidebarApp>
  ))
  .addWithInfo('On the right', () => (
    <FakeSidebarApp>
      <Sidebar title='A sidebar title' position='right'>
        <Lorem count={2} />
      </Sidebar>
    </FakeSidebarApp>
  ))
  .addWithInfo('Larger', () => (
    <FakeSidebarApp>
      <Sidebar title='A sidebar title' width='lg'>
        <Lorem count={2} />
      </Sidebar>
    </FakeSidebarApp>
  ))
  .addWithInfo('Playground', () => {
    const title = text('Title', 'A sidebar title');
    const paragraphs = number('Paragraphs', 2);
    const position = select('Position', ['left', 'right'], 'left');
    const width = select('Width', ['xs', 'sm', 'md', 'lg'], 'md');
    const maximized = boolean('Maximized');
    const loading = boolean('Loading');
    const loaderLabel = text('Loader label');
    const theme = select(
      'Theme',
      [
        'Default',
        'White',
        'LightGray',
        'DarkGray',
        'Anthracite',
        'Yellow',
        'Orange',
        'Brown',
        'Red',
        'Rose',
        'Purple',
        'Blue',
        'Sky',
        'Turquoise',
        'Green'
      ],
      'Default'
    );
    const themes = {
      DefaultTheme,
      WhiteTheme,
      LightGrayTheme,
      DarkGrayTheme,
      AnthraciteTheme,
      YellowTheme,
      OrangeTheme,
      BrownTheme,
      RedTheme,
      RoseTheme,
      PurpleTheme,
      BlueTheme,
      SkyTheme,
      TurquoiseTheme,
      GreenTheme
    };
    const ThemeElement = themes[`${theme}Theme`];

    return (
      <DefaultTheme>
        <KnobsAlert />
        <FakeApp fakeText style={{ top: '80px' }}>
          <ThemeElement>
            <FakeSidebarCenter>
              <Sidebar
                title={title}
                loading={loading}
                loaderLabel={loaderLabel}
                position={position}
                width={width}
                maximized={maximized}
                onOpen={action('onOpen')}
                onClose={action('onClose')}
                onClickClose={action('onClickClose')}
                onClickBack={action('onClickBack')}
                onMaximize={action('onMaximize')}
                onUnmaximize={action('onUnmaximize')}
              >
                <Lorem count={paragraphs} />
              </Sidebar>
            </FakeSidebarCenter>
          </ThemeElement>
        </FakeApp>
      </DefaultTheme>
    );
  });
