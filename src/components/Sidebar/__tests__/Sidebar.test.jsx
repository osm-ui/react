import React from 'react';
import 'jest-styled-components';
import { shallow } from 'enzyme';
import Sidebar from '..';

describe('When using snapshots', () => {
  it('Should be rendered correctly', () => {
    const wrapper = shallow(
      <Sidebar>
        <div />
      </Sidebar>
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('Should be rendered with title', () => {
    const wrapper = shallow(
      <Sidebar title='test'>
        <div />
      </Sidebar>
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('Should be rendered with header', () => {
    const wrapper = shallow(
      <Sidebar header={<div />}>
        <div />
      </Sidebar>
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('Should be rendered with loader', () => {
    const wrapper = shallow(
      <Sidebar loading>
        <div />
      </Sidebar>
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('Should be rendered with footer', () => {
    const wrapper = shallow(
      <Sidebar footer={<div />}>
        <div />
      </Sidebar>
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('Should be rendered with back button', () => {
    const wrapper = shallow(
      <Sidebar onClickBack={jest.fn()}>
        <div />
      </Sidebar>
    );

    expect(wrapper).toMatchSnapshot();
  });
});

describe('When testing initialization', () => {
  it('Should have the correct state', () => {
    const wrapper = shallow(
      <Sidebar>
        <div />
      </Sidebar>
    );

    expect(wrapper.state('opened')).toBe(true);
  });

  it('Should have the correct state when opened prop is false', () => {
    const wrapper = shallow(
      <Sidebar opened={false}>
        <div />
      </Sidebar>
    );

    expect(wrapper.state('opened')).toBe(false);
  });
});

describe('When testing callbacks', () => {
  it('Should call the mounting callbacks when necessary', () => {
    const onOpen = jest.fn();
    const onMaximize = jest.fn();

    shallow(
      <Sidebar onOpen={onOpen} onMaximize={onMaximize} maximized>
        <div />
      </Sidebar>
    );

    expect(onOpen).toBeCalledTimes(1);
    expect(onMaximize).toBeCalledTimes(1);
  });

  it('Should not call the mounting callbacks when not necessary', () => {
    const onOpen = jest.fn();
    const onMaximize = jest.fn();

    shallow(
      <Sidebar onOpen={onOpen} onMaximize={onMaximize} opened={false}>
        <div />
      </Sidebar>
    );

    expect(onOpen).not.toBeCalled();
    expect(onMaximize).not.toBeCalled();
  });

  it('Should call the unmounting callback', () => {
    const onClose = jest.fn();

    const wrapper = shallow(
      <Sidebar onClose={onClose}>
        <div />
      </Sidebar>
    );

    expect(onClose).not.toBeCalled();
    wrapper.unmount();
    expect(onClose).toBeCalledTimes(1);
  });

  it('Should call the onOpen/onClose callbacks when opening/closing', () => {
    const onOpen = jest.fn();
    const onClose = jest.fn();

    const wrapper = shallow(
      <Sidebar onOpen={onOpen} onClose={onClose}>
        <div />
      </Sidebar>
    );

    expect(wrapper.state('opened')).toBe(true);
    expect(onOpen).toBeCalledTimes(1);
    expect(onClose).not.toBeCalled();

    wrapper.find('button.close-btn').simulate('click');

    expect(wrapper.state('opened')).toBe(false);
    expect(onOpen).toBeCalledTimes(1);
    expect(onClose).toBeCalledTimes(1);

    wrapper.setProps({ opened: true });

    expect(wrapper.state('opened')).toBe(true);
    expect(onOpen).toBeCalledTimes(2);
    expect(onClose).toBeCalledTimes(1);

    wrapper.setProps({ opened: false });

    expect(wrapper.state('opened')).toBe(false);
    expect(onOpen).toBeCalledTimes(2);
    expect(onClose).toBeCalledTimes(2);

    wrapper.setProps({ opened: true });

    expect(wrapper.state('opened')).toBe(true);
    expect(onOpen).toBeCalledTimes(3);
    expect(onClose).toBeCalledTimes(2);
  });

  it('Should call the onMaximize/onUnmaximize callbacks when necessary', () => {
    const onMaximize = jest.fn();
    const onUnmaximize = jest.fn();

    const wrapper = shallow(
      <Sidebar maximized onMaximize={onMaximize} onUnmaximize={onUnmaximize}>
        <div />
      </Sidebar>
    );

    expect(onMaximize).toBeCalledTimes(1);
    expect(onUnmaximize).not.toBeCalled();

    wrapper.setProps({ maximized: false });

    expect(onMaximize).toBeCalledTimes(1);
    expect(onUnmaximize).toBeCalledTimes(1);

    wrapper.setProps({ maximized: true });

    expect(onMaximize).toBeCalledTimes(2);
    expect(onUnmaximize).toBeCalledTimes(1);
  });

  it('Should call the onClickBack callback when clicked on back button', () => {
    const onClickBack = jest.fn();

    const wrapper = shallow(
      <Sidebar onClickBack={onClickBack}>
        <div />
      </Sidebar>
    );

    expect(onClickBack).not.toBeCalled();

    wrapper.find('button.back-btn').simulate('click');

    expect(onClickBack).toBeCalledTimes(1);
  });
});
