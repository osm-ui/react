import styled from 'styled-components';

import { getRawTheme } from 'helpers/themes';
import { Header, Footer } from '../Layout';

export const slideDuration = 500;

const StyledSidebar = styled.aside.attrs({
  theme: getRawTheme
})`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  position: absolute;

  z-index: 1000;
  top: 0;
  max-width: 100%;
  height: 100%;
  overflow-y: auto;
  transition: all ${slideDuration}ms ease-out;
  font-size: ${p => p.theme.sidebar.fontSize};
  line-height: ${p => p.theme.sidebar.lineHeight};

  color: ${p => p.theme.color};
  background: ${p => p.theme.backgroundColor};
  box-shadow: ${p => p.theme.sidebar.boxShadow};

  ${Header.style}, ${Footer.style} {
    flex: 0 0 auto;
    display: flex;
    justify-content: space-between;
    align-items: start;
    padding: 0;
  }

  ${Header.style} {
    margin-bottom: 1rem;

    .headerContent {
      padding: 0.5rem 0;
      flex: 1 0 auto;
      text-align: center;
    }
  }

  ${Footer.style} {
    padding: 1rem;
  }

  .content {
    flex: 0 1 auto;
    padding: 0 2rem;
    overflow-y: scroll;
  }

  &.xs {
    width: 15rem;
  }
  &.sm {
    width: 20rem;
  }
  &.md {
    width: 30rem;
  }
  &.lg {
    width: 40rem;
  }
  &.maximized {
    width: 100%;
  }

  &.left {
    left: 0;
    border-right-width: ${p => p.theme.borderWidth};
    transform: translate(-100%, 0);
  }

  &.right {
    right: 0;
    border-left-width: ${p => p.theme.borderWidth};
    transform: translate(100%, 0);
  }

  &.slide-enter,
  &.slide-enter-active,
  &.slide-enter-done {
    transform: translate(0, 0);
  }

  &.maximized {
    border-width: 0;
  }

  .back-btn,
  .close-btn {
    color: ${p => p.theme.controlColor};
    background: transparent;
    border-width: 0;
    outline: none;
    width: 3rem;
    height: 3rem;
    padding: 0;

    &:hover {
      color: ${p => p.theme.hoverControlColor};
    }
  }

  .back-btn {
    margin-right: 0.5rem;
  }

  .close-btn {
    margin-left: 0.5rem;
  }
`;

export default StyledSidebar;
