import styled from 'styled-components';

import { getRawTheme } from 'helpers/themes';

const StyledMap = styled.div.attrs({
  theme: getRawTheme
})`
  .leaflet-bar {
    border: none;
  }

  .leaflet-bar a {
    color: ${props => props.theme.map.controlColor};
    background: ${props => props.theme.map.controlBackgroundColor};
    border: none;
    box-shadow: ${props => props.theme.boxShadow};
    width: 2.2rem;
    height: 2.2rem;
    line-height: 2.2rem;
    transition: all 0.1s ease-out;

    &:hover,
    &:focus,
    &:active {
      color: ${props => props.theme.map.hoverControlColor};
      background: ${props => props.theme.map.hoverControlBackgroundColor};
    }

    &:active {
      box-shadow: inset 0 3px 5px rgba(0, 0, 0, 0.125);
    }
  }

  .leaflet-control-attribution {
    padding: 0.2rem 0.6rem;
  }
`;

export default StyledMap;
