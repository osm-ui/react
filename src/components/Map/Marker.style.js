import { injectGlobal } from 'styled-components';

/* eslint-disable no-unused-expressions */
injectGlobal`
  .osm-ui-react-marker-shape {
    width: 3rem !important;
    height: 3rem !important;
    pointer-events: none;
    cursor: grab;

    svg {
      z-index: 1 !important;
      position: absolute;
      top: 0;
      left: 0;
    }

    .osm-ui-react-marker-icon-wrapper {
      width: 1.4rem;
      height: 1.4rem;
      position: absolute;
      z-index: 2;
      display: flex;
      justify-content: center;
      align-items: center;
      overflow: visible;
      font-size: 1rem;
    }

    svg,
    .osm-ui-react-marker-icon-wrapper {
      pointer-events: auto;
      cursor: pointer;
    }
  }

  .osm-ui-react-marker-pointerClassic,
  .osm-ui-react-marker-pointerClassicThin,
  .osm-ui-react-marker-pointerCirclePin {
    svg {
      top: -2.4rem;
      left: 0;
    }

    .osm-ui-react-marker-icon-wrapper {
      top: -1.7rem;
      left: 0.88rem;
    }
  }

  .osm-ui-react-marker-basicCircle,
  .osm-ui-react-marker-basicSquare,
  .osm-ui-react-marker-basicUpTriangle,
  .osm-ui-react-marker-basicRightTriangle,
  .osm-ui-react-marker-basicDownTriangle,
  .osm-ui-react-marker-basicLeftTriangle,
  .osm-ui-react-marker-basicDiamond {
    .osm-ui-react-marker-icon-wrapper {
      display: none;
    }
  }
`;
/* eslint-enable */
