import React from 'react';
import L from 'leaflet';
import PropTypes from 'prop-types';
import MarkerClusterGroup from 'react-leaflet-markercluster';
import 'react-leaflet-markercluster/dist/styles.min.css';
import { injectGlobal } from 'styled-components';

import { buildClusterSize } from 'helpers/cluster';
import { buildMarkerIconColor } from 'helpers/themes';
import './MarkerClusterGroup.style';

const injectedColors = [];

function buildClusterIcon(theme, base64Color, cluster) {
  const featureCount = cluster.getChildCount();
  const size = buildClusterSize(30, 80, featureCount);

  return L.divIcon({
    className: `osm-ui-react-marker-cluster osm-ui-react-marker-cluster-${theme} ${base64Color}`,
    html: `<div style="width: ${size}px; height: ${size}px">${featureCount}</div>`,
    iconSize: L.point(size, size, true)
  });
}

const MapMarkerClusterGroup = ({ theme, color, ...props }) => {
  let base64Color = '';

  if (color) {
    base64Color = btoa(color).replace(/(=)/g, '');

    if (!injectedColors.includes(base64Color)) {
      injectedColors.push(base64Color);
      const iconColor = buildMarkerIconColor(color);

      /* eslint-disable no-unused-expressions */
      injectGlobal`
        .osm-ui-react-marker-cluster.${base64Color} > div {
          ${color ? `background: ${color};` : ''}
          ${color ? `color: ${iconColor};` : ''}
        }
      `;
    }
  }

  return (
    <MarkerClusterGroup
      iconCreateFunction={buildClusterIcon.bind(this, theme, base64Color)}
      polygonOptions={{
        stroke: false,
        fillColor: '#000',
        fillOpacity: 0.3
      }}
      {...props}
    />
  );
};

MapMarkerClusterGroup.propTypes = {
  theme: PropTypes.string,
  color: PropTypes.string
};

MapMarkerClusterGroup.defaultProps = {
  theme: 'default',
  color: ''
};

MapMarkerClusterGroup.displayName = 'Map.MarkerClusterGroup';

export default MapMarkerClusterGroup;
