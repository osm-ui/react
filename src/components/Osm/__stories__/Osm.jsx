import React from 'react';
import { storiesOf } from '@storybook/react';
import { host } from 'storybook-host';
import { action } from '@storybook/addon-actions';
import { withKnobs } from '@storybook/addon-knobs';
import defaultHostOptions from 'helpers/__stories__/defaultHostOptions';

import { DefaultTheme, Editor } from 'index';

import { osmData } from 'helpers/__mocks__/osm';

const actions = {
  selectField: action('select'),
  addField: action('add'),
  revertField: action('revert'),
  modifyField: action('modify'),
  removeField: action('remove')
};

storiesOf('Osm', module)
  .addDecorator(withKnobs)
  .addDecorator(
    host({
      ...defaultHostOptions,
      title: 'Osm'
    })
  )
  .addWithInfo('Field', () => (
    <DefaultTheme>
      <Editor.Field tag='original' value='Value is unchanged' {...actions} />
      <Editor.Field
        tag='changed'
        status='mod'
        value='Value has changed'
        {...actions}
      />
      <Editor.Field tag='new' status='add' value='Field is new' {...actions} />
      <Editor.Field
        tag='removed'
        status='del'
        value='Field has been removed'
        {...actions}
      />
      <Editor.Field tag='emptyTag' {...actions} />
      <Editor.Field tag='inactiveTag' inactive {...actions} />
    </DefaultTheme>
  ))
  .addWithInfo('Editor', () => (
    <DefaultTheme>
      <Editor
        original={osmData.original}
        fixed={osmData.fixed}
        submit={action('submit')}
      />
    </DefaultTheme>
  ));
