import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Transition from 'react-transition-group/Transition';

import Button from 'components/Button';
import Form from 'components/Form';

import StyledField from './Field.style';

const placeholder = 'A value is required';

export const statusValue = {
  ADD: 'add',
  MOD: 'mod',
  DEL: 'del'
};

const transitionStyles = {
  entered: {
    opacity: 1
  }
};

class Field extends React.Component {
  componentDidUpdate({ value: prevValue }) {
    const { selected, value } = this.props;

    if (this.input && value !== prevValue) {
      if (selected) this.input.focus();
      else this.input.blur();

      if (value && this.input.type === 'text') {
        const position = value.length;
        this.input.setSelectionRange(position, position);
      }
    }
  }

  renderInput = () => {
    const { tag, value, modifyField } = this.props;

    const isValueTooLong = value && value.length > 15;

    return isValueTooLong ? (
      <Form.Textarea
        value={value}
        placeholder={placeholder}
        innerRef={input => {
          this.input = input;
        }}
        onChange={event => modifyField && modifyField(tag, event.target.value)}
        // we should probably add an onkeydown callback to close input on Enter
      />
    ) : (
      <Form.Input
        value={value || ''}
        placeholder={placeholder}
        innerRef={input => {
          this.input = input;
        }}
        onChange={event => modifyField && modifyField(tag, event.target.value)}
        // we should probably add an onkeydown callback to close input on Enter
      />
    );
  };

  renderActions = () => {
    const { tag, status, selected, revertField, removeField } = this.props;

    return (
      <Transition
        in={selected}
        timeout={{
          enter: 0,
          exit: 300
        }}
        appear
        unmountOnExit
      >
        {state => (
          <div className='actions' style={transitionStyles[state]}>
            {status === statusValue.MOD && (
              <Button
                shape='square'
                size='sm'
                onClick={() => revertField && revertField(tag)}
                className='action mod'
              >
                <i className='fas fa-undo' />
              </Button>
            )}
            {status !== statusValue.DEL && (
              <Button
                shape='square'
                size='sm'
                onClick={() => removeField && removeField(tag)}
                className='action del'
              >
                <i className='fas fa-times' />
              </Button>
            )}
          </div>
        )}
      </Transition>
    );
  };

  render() {
    const {
      tag,
      value,
      status,
      selected,
      inactive,
      addField,
      selectField,
      className
    } = this.props;

    const isRemoved = status === statusValue.DEL;

    const classNames = classnames('osmField', className, status, {
      selected,
      inactive,
      removed: isRemoved,
      missing: !isRemoved && !value
    });

    return (
      <StyledField className={classNames}>
        <div className='main'>
          <Button
            active={selected}
            shape='square'
            size='sm'
            className='tag'
            onClick={() => {
              if (isRemoved && addField) addField(tag, value);
              else if (!inactive && selectField) selectField(tag);
            }}
          >
            {tag}
          </Button>
          <div className='value'>{value || placeholder}</div>
          {this.renderActions()}
        </div>
        {!isRemoved && this.renderInput()}
      </StyledField>
    );
  }
}

Field.propTypes = {
  tag: PropTypes.string.isRequired,
  addField: PropTypes.func,
  revertField: PropTypes.func,
  modifyField: PropTypes.func,
  removeField: PropTypes.func,
  selectField: PropTypes.func,
  status: PropTypes.string,
  value: PropTypes.string,
  selected: PropTypes.bool,
  inactive: PropTypes.bool,
  className: PropTypes.string
};

Field.defaultProps = {
  status: null,
  value: null,
  selected: false,
  inactive: false,
  addField: null,
  revertField: null,
  modifyField: null,
  removeField: null,
  selectField: null,
  className: ''
};

Field.displayName = 'Field';
Field.style = StyledField;

export default Field;
