import styled from 'styled-components';

import Button from 'components/Button';
import Form from 'components/Form';
import { colors } from 'constants/index';

const StyledField = styled.div`
  margin-bottom: 1rem;
  transition: opacity 300ms ease-in-out;

  .main {
    z-index: 1;
    position: relative;
    background-color: ${colors.white};
    display: flex;
    align-items: center;

    ${Button.style} {
      padding-left: 1rem;
      padding-right: 1rem;
      border-radius: 2px 0 0 2px;

      &:focus {
        outline: none;
      }
    }

    .value {
      flex: 1 0 10rem;
      line-height: 1.6rem;
      padding: 1px 1rem;
      border: 1px solid ${colors.lightGray3};
      border-left: none;
      border-radius: 0 2px 2px 0;
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    }

    .actions {
      position: absolute;
      opacity: 0;
      display: flex;
      right: 0;
      top: 0;
      bottom: 0;

      transition: opacity 300ms ease-in-out;

      ${Button.style} {
        padding: 0 1rem;
      }

      .action {
        color: ${colors.white};
        line-height: 1.6rem;
        border: 2px solid transparent;
        cursor: pointer;
      }

      .del {
        background-color: ${colors.red2};
        border-color: ${colors.red1};
        border-radius: 0 2px 0 0;
      }

      .mod {
        background-color: ${colors.orange2};
        border-color: ${colors.orange1};
        border-radius: 0;
      }
    }
  }

  &.add ${Button.style}, &.add .value {
    border-color: ${colors.green3};
  }

  &.add ${Button.style} {
    color: ${colors.white};
    border-color: ${colors.green1};
    background-color: ${colors.green3};
  }

  &.mod ${Button.style}, &.mod .value {
    border-color: ${colors.orange3};
  }

  &.mod ${Button.style} {
    color: ${colors.white};
    border-color: ${colors.orange1};
    background-color: ${colors.orange2};
  }

  &.del ${Button.style}, &.del .value {
    border-color: ${colors.red3};
  }

  &.del ${Button.style} {
    color: ${colors.white};
    border-color: ${colors.red1};
    background-color: ${colors.red3};
  }

  &.missing .value {
    color: ${colors.red1};
    font-style: italic;
  }

  &.inactive,
  &.removed {
    opacity: 0.2;

    ${Form.Input.wrapperStyle}, ${Form.Textarea.wrapperStyle} {
      visibility: hidden;
    }
  }

  ${Form.Input.wrapperStyle}, ${Form.Textarea.wrapperStyle} {
    height: 1.6rem;
    z-index: 0;
    margin-top: -1.6rem;
    transition: margin-top 300ms ease-in-out, height 300ms ease-in-out;

    input,
    textarea {
      height: 100%;
    }
  }

  ${Form.Input.style}, ${Form.Textarea.style} {
    line-height: 1.7rem;
    border-radius: 0 0 2px 2px;
  }

  &.selected {
    opacity: 1;

    ${Button.style} {
      border-radius: 2px 0 0 0;
    }

    ${Form.Input.wrapperStyle}, ${Form.Textarea.wrapperStyle} {
      margin-top: 0;
    }

    ${Form.Textarea.wrapperStyle} {
      height: 4rem;
    }

    .value {
      border-radius: 0 2px 0 0;
    }
  }
`;

export default StyledField;
