import React from 'react';
import 'jest-styled-components';
import { shallow, mount } from 'enzyme';
import { snapshotWithElementChildren } from 'helpers/tests';
import { statusValue } from '../Field';
import Editor from '..';
import { osmData } from 'helpers/__mocks__/osm';

describe('When using snapshots', () => {
  it('Should render with an element children', () =>
    snapshotWithElementChildren(Editor, {
      original: osmData.original,
      fixed: osmData.fixed,
      submit: jest.fn
    }));
});

describe('When testing initialization', () => {
  it('Should render with the correct default classnames', () => {
    const wrapper = shallow(<Editor fixed={{}} submit={jest.fn()} />);

    expect(wrapper.hasClass('osmEditor')).toBe(true);
  });

  it('Should render as many non removed items as provided', () => {
    const original = {
      a: 'unchanged',
      b: 'unchanged',
      c: 'unchanged',
      d: 'changed',
      e: 'changed',
      f: 'removed',
      k: 'removed'
    };
    const fixed = {
      a: 'unchanged',
      b: 'unchanged',
      c: 'unchanged',
      d: 'test',
      e: 'test',
      g: 'new',
      h: 'new',
      i: 'new',
      j: 'new'
    };

    const wrapper = mount(
      <Editor original={original} fixed={fixed} submit={jest.fn()} />
    );

    expect(wrapper.find(`div.osmField.${statusValue.ADD}`).length).toBe(4);
    expect(wrapper.find(`div.osmField.${statusValue.DEL}`).length).toBe(2);
    expect(wrapper.find(`div.osmField.${statusValue.MOD}`).length).toBe(2);
    expect(wrapper.find('div.osmField').length).toBe(11);
  });

  it('Non-selected items should not have action buttons', () => {
    const fixed = {
      a: 'new'
    };

    const wrapper = mount(<Editor fixed={fixed} submit={jest.fn()} />);
    expect(wrapper.find('div.osmField').hasClass('selected')).toBe(false);
    expect(wrapper.find('div.osmField .actions').length).toBe(0);
  });
});

describe('When testing interactions', () => {
  it('Should toggle the input when clicked on the + button with no new tag', () => {
    const wrapper = mount(<Editor fixed={{}} submit={jest.fn()} />);
    const button = wrapper.find('.add-button-container button');

    button.simulate('click');

    expect(wrapper.state('isAdding')).toBe(true);
    expect(wrapper.find('.add-item').hasClass('open')).toBe(true);

    button.simulate('click');

    expect(wrapper.state('isAdding')).toBe(false);
    expect(wrapper.find('.add-item').hasClass('open')).toBe(false);
  });

  it('Should not open the input when clicked on the + button with a selected tag', () => {
    const wrapper = mount(<Editor fixed={{}} submit={jest.fn()} />);

    wrapper.setState({ selectedTag: 'a' });

    wrapper.find('.add-button-container button').simulate('click');

    expect(wrapper.state('isAdding')).toBe(false);
    expect(wrapper.find('.add-item').hasClass('open')).toBe(false);
  });

  it('Should add tag when clicked on the + button with a new tag name', () => {
    const wrapper = mount(<Editor fixed={{}} submit={jest.fn()} />);

    wrapper.setState({ isAdding: true });
    wrapper.instance().add.value = 'test';

    wrapper.find('.add-button-container button').simulate('click');

    expect(wrapper.state('fixed')).toHaveProperty('test', undefined);
    expect(wrapper.state('isAdding')).toBe(false);
    expect(wrapper.find('.add-item').hasClass('open')).toBe(false);
  });

  it('Should modify new tag when updating add input value', () => {
    const wrapper = mount(<Editor fixed={{}} submit={jest.fn()} />);

    wrapper.setState({ isAdding: true });

    wrapper
      .find('.add-item input')
      .simulate('change', { target: { value: 'new' } });

    expect(wrapper.state('newTag')).toBe('new');
  });

  it('Should select / unselect item when user clicks on its name', () => {
    const fixed = {
      a: 'new'
    };

    const wrapper = mount(<Editor fixed={fixed} submit={jest.fn()} />);

    const tag = wrapper.find('div.osmField .tag').first();
    tag.simulate('click');

    expect(wrapper.state('selectedTag')).toBe('a');
    expect(wrapper.find('div.osmField.selected').length).toBe(1);
    expect(wrapper.find('div.osmField.selected .actions').length).toBe(1);

    tag.simulate('click');

    expect(wrapper.state('selectedTag')).toBe(null);
    expect(wrapper.find('div.osmField.selected').length).toBe(0);
  });

  it('Should remove tag when clicked on the x button of a selected tag', () => {
    const original = {
      a: 'unchanged'
    };

    const wrapper = mount(
      <Editor original={original} fixed={original} submit={jest.fn()} />
    );

    wrapper.setState({ selectedTag: 'a' });

    wrapper.find('.selected button.action.del').simulate('click');

    expect(wrapper.find('div.osmField.removed').length).toBe(1);
    expect(wrapper.find('div.selected').length).toBe(0);
    expect(wrapper.state('fixed')).not.toHaveProperty('a');
  });

  it('Should re-add tag when clicked on a removed tag', () => {
    const original = {
      a: 'unchanged'
    };

    const wrapper = mount(
      <Editor original={original} fixed={{}} submit={jest.fn()} />
    );

    wrapper.find('.removed button.tag').simulate('click');

    expect(wrapper.find('div.osmField').length).toBe(1);
    expect(wrapper.find('div.osmField.removed').length).toBe(0);
    expect(wrapper.state('fixed')).toHaveProperty('a');
    expect(wrapper.state('fixed').a).toEqual('unchanged');
  });

  it('Should revert tag when clicked on the revert button of a selected tag', () => {
    const original = {
      a: 'unchanged'
    };

    const fixed = {
      a: 'changed'
    };

    const wrapper = mount(
      <Editor original={original} fixed={fixed} submit={jest.fn()} />
    );

    wrapper.setState({ selectedTag: 'a' });

    wrapper.find('.selected button.action.mod').simulate('click');

    expect(wrapper.find('div.osmField.mod').length).toBe(0);
    expect(wrapper.find('div.selected').length).toBe(0);
    expect(wrapper.state('fixed').a).toEqual(original.a);
  });

  it('Should modify tag value when modified input of a selected tag', () => {
    const original = {
      a: 'unchanged'
    };

    const fixed = {
      a: 'unchanged'
    };

    const wrapper = mount(
      <Editor original={original} fixed={fixed} submit={jest.fn()} />
    );

    wrapper.setState({ selectedTag: 'a' });

    wrapper
      .find('.selected input')
      .simulate('change', { target: { value: 'changed' } });

    expect(wrapper.find('div.osmField.mod').length).toBe(1);
    expect(wrapper.state('fixed').a).toEqual('changed');
  });

  it('Should call the submit callback when clicked on submit button', () => {
    const callback = jest.fn();
    const fixed = {
      a: 'unchanged'
    };

    const wrapper = mount(<Editor fixed={fixed} submit={callback} />);
    wrapper.find('button.submit').simulate('click');

    expect(callback).toHaveBeenCalledTimes(1);
  });

  it('Should not call the submit callback when clicked on submit button if conditions are not met', () => {
    const callback = jest.fn();
    const fixed = {
      a: undefined
    };

    const wrapper = mount(<Editor fixed={fixed} submit={callback} />);
    const submitBtn = wrapper.find('button.submit');

    wrapper.setState({ isAdding: true });

    submitBtn.simulate('click');
    expect(callback).not.toHaveBeenCalled();

    wrapper.setState({
      isAdding: false,
      selectedTag: 'a'
    });

    submitBtn.simulate('click');
    expect(callback).not.toHaveBeenCalled();

    wrapper.setState({
      selectedTag: null
    });

    submitBtn.simulate('click');
    expect(callback).not.toHaveBeenCalled();
  });
});
