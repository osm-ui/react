import React from 'react';
import 'jest-styled-components';
import { shallow, mount } from 'enzyme';
import { snapshotWithElementChildren } from 'helpers/tests';
import { statusValue } from '../Field';
import { Editor, Button, Form } from 'index';

describe('When using snapshots', () => {
  it('Should render with an element children', () =>
    snapshotWithElementChildren(Editor.Field, { tag: 'tag' }));
});

describe('When testing initialization', () => {
  it('Should render with the correct default classnames', () => {
    const wrapper = shallow(<Editor.Field tag={'tag'} />);

    expect(wrapper.hasClass('osmField')).toBe(true);
    expect(wrapper.hasClass('selected')).toBe(false);
    expect(wrapper.hasClass('inactive')).toBe(false);
    expect(wrapper.hasClass('removed')).toBe(false);
    expect(wrapper.hasClass('missing')).toBe(true);
  });

  it('Should render with the correct classnames', () => {
    const wrapper = shallow(
      <Editor.Field
        className='test'
        selected
        inactive
        status={statusValue.DEL}
        tag={'tag'}
      />
    );

    expect(wrapper.hasClass('test')).toBe(true);
    expect(wrapper.hasClass(statusValue.DEL)).toBe(true);
    expect(wrapper.hasClass('selected')).toBe(true);
    expect(wrapper.hasClass('inactive')).toBe(true);
    expect(wrapper.hasClass('removed')).toBe(true);
    expect(wrapper.hasClass('missing')).toBe(false);
  });

  it('Should render with a Form.Input when given a less than 16-long input value', () => {
    const wrapper = shallow(<Editor.Field value='1' tag={'tag'} />);

    expect(wrapper.find(Form.Input).exists()).toBe(true);
    expect(wrapper.find(Form.Textarea).exists()).toBe(false);
  });

  it('Should render with a Form.Textarea when given a 16-long input value', () => {
    const wrapper = shallow(
      <Editor.Field value='1234567890123456' tag={'tag'} />
    );

    expect(wrapper.find(Form.Input).exists()).toBe(false);
    expect(wrapper.find(Form.Textarea).exists()).toBe(true);
  });

  it('Should render with no action button when not given selected prop', () => {
    const wrapper = mount(<Editor.Field tag={'tag'} />);

    expect(wrapper.find('.actions').exists()).toBe(false);
  });

  it('Should render with delete action button when given selected prop', () => {
    const wrapper = mount(<Editor.Field tag={'tag'} selected />);

    expect(wrapper.find('.action.del').exists()).toBe(true);
  });

  it('Should render with revert action button when given selected prop and the MOD status', () => {
    const wrapper = mount(
      <Editor.Field tag={'tag'} selected status={statusValue.MOD} />
    );

    expect(wrapper.find('.action.mod').exists()).toBe(true);
  });
});

describe('When testing callbacks', () => {
  it('Should call the addField callback when clicked on a removed Field Button', () => {
    const callback = jest.fn();
    const wrapper = shallow(
      <Editor.Field tag={'tag'} status={statusValue.DEL} addField={callback} />
    );

    wrapper.find(Button).simulate('click');
    expect(callback).toHaveBeenCalledTimes(1);
  });

  it('Should call the selectField callback when clicked on an unselected Field Button', () => {
    const callback = jest.fn();
    const wrapper = shallow(
      <Editor.Field tag={'tag'} selectField={callback} />
    );

    wrapper.find(Button).simulate('click');
    expect(callback).toHaveBeenCalledTimes(1);
  });

  it('Should not call the selectField callback when clicked on an unselected Field Button', () => {
    const callback = jest.fn();
    const wrapper = shallow(
      <Editor.Field tag={'tag'} inactive selectField={callback} />
    );

    wrapper.find(Button).simulate('click');
    expect(callback).not.toHaveBeenCalled();
  });

  it('Should call the removeField callback when clicked on a selected Field remove Button', () => {
    const callback = jest.fn();
    const wrapper = mount(
      <Editor.Field tag={'tag'} selected removeField={callback} />
    );

    wrapper
      .find('.action.del')
      .at(0)
      .simulate('click');
    expect(callback).toHaveBeenCalledTimes(1);
  });

  it('Should call the revertField callback when clicked on a modified Field revert Button', () => {
    const callback = jest.fn();
    const wrapper = mount(
      <Editor.Field
        tag={'tag'}
        selected
        status={statusValue.MOD}
        revertField={callback}
      />
    );

    wrapper
      .find('.action.mod')
      .at(0)
      .simulate('click');
    expect(callback).toHaveBeenCalledTimes(1);
  });

  it('Should call the modifyField callback when changed the input', () => {
    const callback = jest.fn();
    const wrapper = mount(
      <Editor.Field tag={'tag'} selected modifyField={callback} />
    );

    wrapper.find('input').simulate('change', { target: { value: 'test' } });
    expect(callback).toHaveBeenCalledTimes(1);
  });

  it('Should call the modifyField callback when changed the textarea', () => {
    const callback = jest.fn();
    const wrapper = mount(
      <Editor.Field
        tag={'tag'}
        value='1234567890123456'
        selected
        modifyField={callback}
      />
    );

    wrapper.find('textarea').simulate('change', { target: { value: 'test' } });
    expect(callback).toHaveBeenCalledTimes(1);
  });
});

describe('When testing other behaviours', () => {
  it('Should call the blur method of input when a prop is updated', () => {
    const wrapper = mount(<Editor.Field tag={'tag'} />);

    const input = wrapper.instance().input;
    jest.spyOn(input, 'blur');

    wrapper.setProps({ value: 'test' });
    expect(input.blur).toHaveBeenCalled();
  });

  it('Should call the blur method of input when not selected and value prop is updated', () => {
    const wrapper = mount(<Editor.Field tag={'tag'} />);

    const input = wrapper.instance().input;
    jest.spyOn(input, 'blur');

    wrapper.setProps({ value: 'test' });
    expect(input.blur).toHaveBeenCalled();
  });

  it('Should call the focus method of input when selected and value prop is updated', () => {
    const wrapper = mount(<Editor.Field tag={'tag'} selected />);

    const input = wrapper.instance().input;
    jest.spyOn(input, 'focus');

    wrapper.setProps({ value: 'test' });
    expect(input.focus).toHaveBeenCalled();
  });

  it('Should call the setSelectionRange method of input when value prop is updated', () => {
    const wrapper = mount(<Editor.Field tag={'tag'} />);

    const input = wrapper.instance().input;
    jest.spyOn(input, 'setSelectionRange');

    wrapper.setProps({ value: 'test' });
    expect(input.setSelectionRange).toHaveBeenCalled();
  });

  it('Should not call the setSelectionRange method of input when value prop is updated with null', () => {
    const wrapper = mount(
      <Editor.Field tag={'tag'} value='1234567890123456' />
    );

    const input = wrapper.instance().input;
    jest.spyOn(input, 'setSelectionRange');

    wrapper.setProps({ value: null });
    expect(input.setSelectionRange).not.toHaveBeenCalled();
  });

  it('Should not call blur/focus methods of input when value prop has not changed', () => {
    const wrapper = mount(<Editor.Field tag={'tag'} value='test' />);

    const input = wrapper.instance().input;
    jest.spyOn(input, 'blur');
    jest.spyOn(input, 'focus');

    wrapper.setProps({ value: 'test' });
    expect(input.blur).not.toHaveBeenCalled();
    expect(input.focus).not.toHaveBeenCalled();
  });
});
