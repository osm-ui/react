import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import Button from 'components/Button';
import Form from 'components/Form';
import Field, { statusValue as status } from './Field';

import StyledEditor from './Editor.style';

class Editor extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      fixed: props.fixed,
      selectedTag: null,
      isAdding: false
    };
  }

  findStatus = tag => {
    // maybe make it an util and write a test
    const originalTags = Object.keys(this.props.original);
    const fixedTags = Object.keys(this.state.fixed);

    if (!originalTags.includes(tag)) return status.ADD;
    if (!fixedTags.includes(tag)) return status.DEL;
    if (this.props.original[tag] !== this.state.fixed[tag]) return status.MOD;

    return '';
  };

  addField = (tag, value) => {
    var fixed = { ...this.state.fixed };
    fixed[tag] = value;

    this.setState({
      fixed
    });
  };

  removeField = tag => {
    var fixed = { ...this.state.fixed };
    Reflect.deleteProperty(fixed, tag);

    this.setState({
      fixed,
      selectedTag: null
    });
  };

  revertField = tag => {
    var fixed = { ...this.state.fixed };
    fixed[tag] = this.props.original[tag];

    this.setState({
      fixed,
      selectedTag: null
    });
  };

  modifyField = (tag, value) => {
    var fixed = { ...this.state.fixed };
    fixed[tag] = value;

    this.setState({
      fixed
    });
  };

  selectField = tag => {
    const isSelected = tag === this.state.selectedTag;

    this.setState({
      selectedTag: isSelected ? null : tag
    });
  };

  toggleAddMode = () => {
    if (!this.state.isAdding) this.add.focus();
    else this.add.blur();

    this.setState({
      selectedTag: null,
      isAdding: !this.state.isAdding
    });
  };

  addTag = tag => {
    this.addField(tag);
    this.toggleAddMode();
    this.setState(
      {
        newTag: null
      },
      () => this.selectField(tag)
    );
  };

  modifyNewTag = value => {
    this.setState({
      newTag: value
    });
  };

  validate = () => {
    const fixed = this.state.fixed;
    return Object.keys(fixed)
      .map(tag => fixed[tag])
      .every(value => value);
  };

  handleSubmit = () => {
    if (this.validate() && !this.state.selectedTag && !this.isAdding)
      this.props.submit(this.state.fixed);
  };

  renderAddButton = () => {
    const { newTag, selectedTag, isAdding } = this.state;
    const hasSelected = selectedTag !== null;

    const className = classnames('add-item', {
      inactive: hasSelected,
      open: isAdding
    });

    return (
      <div className={className}>
        <Form.Input
          value={newTag || ''}
          placeholder={this.props.newTagText}
          innerRef={input => {
            this.add = input;
          }}
          onChange={e => this.modifyNewTag(e.target.value)}
        />
        <div className='add-button-container'>
          <Button
            shape='square'
            size='sm'
            context='primary'
            onClick={() => {
              if (isAdding && this.add.value) this.addTag(this.add.value);
              else if (!hasSelected) this.toggleAddMode();
            }}
          >
            <i className='fas fa-plus' />
          </Button>
        </div>
      </div>
    );
  };

  renderRemoved = removed => {
    const { original, removedText } = this.props;

    const removedFields = removed
      .sort((a, b) => a.localeCompare(b))
      .map(tag => (
        <Field
          tag={tag}
          status={this.findStatus(tag)}
          value={original[tag]}
          isSelected={false}
          isInactive
          addField={this.addField}
          key={tag}
        />
      ));

    return (
      <div className='removedList'>
        <div>{removedText}</div>
        {removedFields}
      </div>
    );
  };

  render() {
    const { original, submitText } = this.props;
    const { fixed, selectedTag } = this.state;
    const fixedTags = Object.keys(fixed);

    const fields = fixedTags
      .sort((a, b) => a.localeCompare(b))
      .map(tag => (
        <Field
          tag={tag}
          status={this.findStatus(tag)}
          value={fixed[tag]}
          originalValue={original[tag]}
          selected={selectedTag === tag}
          inactive={selectedTag && selectedTag !== tag}
          revertField={this.revertField}
          modifyField={this.modifyField}
          selectField={this.selectField}
          removeField={this.removeField}
          key={tag}
        />
      ));

    const removed = Object.keys(original).filter(
      tag => !fixedTags.includes(tag)
    );

    const valid = this.validate();
    const submitClasses = classnames('submit', {
      inactive: !valid
    });

    return (
      <StyledEditor className='osmEditor'>
        <Form>{fields}</Form>
        {this.renderAddButton()}
        {removed.length > 0 && this.renderRemoved(removed)}
        <Button
          block
          active={valid}
          onClick={this.handleSubmit}
          className={submitClasses}
        >
          {submitText}
        </Button>
      </StyledEditor>
    );
  }
}

Editor.propTypes = {
  submit: PropTypes.func.isRequired,
  fixed: PropTypes.object.isRequired,
  original: PropTypes.object,
  newTagText: PropTypes.string,
  removedText: PropTypes.string,
  submitText: PropTypes.string,
  className: PropTypes.string
};

Editor.defaultProps = {
  original: {},
  newTagText: 'Enter tagname',
  removedText: 'Removed items',
  submitText: 'Validate',
  className: ''
};

Editor.displayName = 'Editor';
Editor.style = StyledEditor;

export default Editor;
