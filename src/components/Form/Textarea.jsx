import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import Hint from './Hint';
import StyledTextarea, { Wrapper } from './styles/Textarea.style';

const Textarea = ({
  rows,
  disabled,
  hint,
  className,
  innerRef,
  style,
  ...props
}) => (
  <Wrapper style={style}>
    <StyledTextarea
      className={classnames(className, 'input')}
      rows={rows}
      disabled={disabled}
      innerRef={innerRef}
      hasHint={!!hint}
      {...props}
    />
    {!!hint && (
      <Hint disabled={disabled} {...props}>
        {hint}
      </Hint>
    )}
  </Wrapper>
);

Textarea.propTypes = {
  rows: PropTypes.number,
  disabled: PropTypes.bool,
  hint: PropTypes.string,
  resize: PropTypes.oneOf(['none', 'vertical', 'horizontal']),
  innerRef: PropTypes.func,
  className: PropTypes.string,
  style: PropTypes.object
};

Textarea.defaultProps = {
  rows: 6,
  disabled: false,
  hint: '',
  resize: 'vertical',
  innerRef: null,
  className: '',
  style: null
};

Textarea.displayName = 'Form.Textarea';
Textarea.wrapperStyle = Wrapper;
Textarea.style = StyledTextarea;

export default Textarea;
