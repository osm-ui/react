import React from 'react';
import 'jest-styled-components';
import { shallow, mount } from 'enzyme';
import { snapshotWithoutChildren } from 'helpers/tests';
import { config } from 'themes/Default';

import Form from '..';

describe('When using snapshots', () => {
  it('Checkbox should render with an element children', () =>
    snapshotWithoutChildren(Form.Checkbox, {
      id: '1',
      name: 'test',
      value: 'myValue'
    }));
});

describe('When testing initialization', () => {
  it('Should render with correct classnames', () => {
    const wrapper = shallow(
      <Form.Checkbox className='test' id='1' name='test' value='myValue' />
    );

    expect(wrapper.hasClass('test')).toBe(true);
  });
});

describe('When testing props', () => {
  it('Should render with correct inner props', () => {
    const wrapper = mount(
      <Form.Checkbox id='1' value='myValue' name='test' theme={config} />
    );

    expect(wrapper.find(Form.Checkbox.style).prop('id')).toBe(
      wrapper.find(Form.Label).prop('htmlFor')
    );
  });
});
