import React from 'react';
import 'jest-styled-components';
import { shallow, mount } from 'enzyme';
import { snapshotWithElementChildren } from 'helpers/tests';
import { config } from 'themes/Default';

import Form from '..';

describe('When using snapshots', () => {
  it('Radio should render with an element children', () =>
    snapshotWithElementChildren(Form.Radio, {
      id: '1',
      value: 'myValue',
      name: 'myName'
    }));
});

describe('When testing initialization', () => {
  it('Should render with correct classnames', () => {
    const wrapper = shallow(
      <Form.Radio className='test' id='1' value='myValue' name='myName' />
    );

    expect(wrapper.hasClass('test')).toBe(true);
  });
});

describe('When testing props', () => {
  it('Should render with correct inner props', () => {
    const wrapper = mount(
      <Form.Radio id='1' value='myValue' name='myName' theme={config} />
    );

    expect(wrapper.find(Form.Radio.style).prop('id')).toBe(
      wrapper.find(Form.Label).prop('htmlFor')
    );
  });
});
