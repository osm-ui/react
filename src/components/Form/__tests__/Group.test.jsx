import React from 'react';
import 'jest-styled-components';
import { shallow } from 'enzyme';
import { snapshotWithElementChildren } from 'helpers/tests';
import Form from '..';

describe('When using snapshots', () => {
  it('Group should render with an element children', () =>
    snapshotWithElementChildren(Form.Group));
});

describe('When testing initialization', () => {
  it('Should render with correct classnames', () => {
    const wrapper = shallow(<Form.Group className='test' context='info' />);

    expect(wrapper.hasClass('test')).toBe(true);
    expect(wrapper.hasClass('has-info')).toBe(true);
  });

  it('Should render with correct props', () => {
    const wrapper = shallow(<Form.Group test='1' />);

    expect(wrapper.prop('test')).toBe('1');
  });
});
