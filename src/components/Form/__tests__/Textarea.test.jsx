import React from 'react';
import 'jest-styled-components';
import { shallow, mount } from 'enzyme';
import { snapshotWithoutChildren } from 'helpers/tests';
import { config } from 'themes/Default';

import Form from '..';

describe('When using snapshots', () => {
  it('TextArea should render with an element children', () =>
    snapshotWithoutChildren(Form.Textarea));

  it('Input with Hint should render with an element children', () =>
    snapshotWithoutChildren(Form.Textarea, { hint: '' }));
});

describe('When testing initialization', () => {
  it('Should render with correct classnames', () => {
    const wrapper = shallow(<Form.Textarea className='test' />);

    expect(wrapper.find(Form.Textarea.style).hasClass('input')).toBe(true);
    expect(wrapper.find(Form.Textarea.style).hasClass('test')).toBe(true);
  });
});

describe('When testing hint', () => {
  it('Should render with correct hint content', () => {
    const wrapper = mount(<Form.Textarea hint='hint' theme={config} />);

    expect(wrapper.find(Form.Hint).text()).toBe('hint');
  });
});
