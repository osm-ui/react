import React from 'react';
import 'jest-styled-components';
import { shallow } from 'enzyme';
import { snapshotWithElementChildren } from 'helpers/tests';
import Form from '..';

describe('When using snapshots', () => {
  it('Form should render with an element children', () =>
    snapshotWithElementChildren(Form));
});

describe('When testing initialization', () => {
  it('Should render with correct classnames', () => {
    const wrapper = shallow(<Form className='test' />);

    expect(wrapper.hasClass('test')).toBe(true);
  });

  it('Should render with correct props', () => {
    const wrapper = shallow(<Form test='1' />);

    expect(wrapper.prop('test')).toBe('1');
  });
});
