import 'jest-styled-components';
import { snapshotWithoutChildren } from 'helpers/tests';

import Form from '..';

describe('When using snapshots', () => {
  it('Select should render with an element children', () =>
    snapshotWithoutChildren(Form.Select));
});
