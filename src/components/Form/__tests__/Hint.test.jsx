import React from 'react';
import 'jest-styled-components';
import { shallow } from 'enzyme';
import { snapshotWithElementChildren } from 'helpers/tests';
import { config } from 'themes/Default';

import Form from '..';

describe('When using snapshots', () => {
  it('Hint should render with an element children', () =>
    snapshotWithElementChildren(Form.Hint));
});

describe('When testing initialization', () => {
  it('Should render with correct default classnames', () => {
    const wrapper = shallow(<Form.Hint theme={config} />);

    expect(wrapper.hasClass('hint')).toBe(true);
  });
});
