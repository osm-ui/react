import React from 'react';
import 'jest-styled-components';
import { shallow, mount } from 'enzyme';
import { snapshotWithoutChildren } from 'helpers/tests';
import { config } from 'themes/Default';

import Form from '..';

describe('When using snapshots', () => {
  it('Input should render with an element children', () =>
    snapshotWithoutChildren(Form.Input));

  it('Input with Hint should render with an element children', () =>
    snapshotWithoutChildren(Form.Input, { hint: 'hint' }));
});

describe('When testing initialization', () => {
  it('Should render with correct classnames', () => {
    const wrapper = shallow(<Form.Input className='test' />);

    expect(wrapper.find(Form.Input.style).hasClass('input')).toBe(true);
    expect(wrapper.find(Form.Input.style).hasClass('test')).toBe(true);
  });
});

describe('When testing hint', () => {
  it('Should render with correct hint content', () => {
    const wrapper = mount(<Form.Input hint='hint' theme={config} />);

    expect(wrapper.find(Form.Hint).text()).toBe('hint');
  });
});
