import Form from './Form';
import Group from './Group';
import Label from './Label';
import Input from './Input';
import Hint from './Hint';
import Textarea from './Textarea';
import Select from './Select';
import Checkbox from './Checkbox';
import Radio from './Radio';

Form.Group = Group;
Form.Label = Label;
Form.Input = Input;
Form.Hint = Hint;
Form.Textarea = Textarea;
Form.Select = Select;
Form.Checkbox = Checkbox;
Form.Radio = Radio;

export default Form;
