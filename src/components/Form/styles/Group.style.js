import styled from 'styled-components';

import { getRawTheme } from 'helpers/themes';

export const contexts = ['', 'info', 'success', 'warning', 'error'];

const colorsStyle = props =>
  contexts.reduce((reducedStyles, context) => {
    if (!context) {
      return reducedStyles;
    }

    const labelColors = props.theme.form.label[context];
    const inputColors = props.theme.form.input[context];
    const hintColors = props.theme.form.hint[context];

    return `
      ${reducedStyles}

      &.has-${context} {
        .control-label {
            color: ${labelColors.color};
        }

        .Select-control,
        .input,
        .textarea {
            background-color: ${inputColors.backgroundColor};
            border-color: ${inputColors.borderColor};
        }

        .hint {
            background-color: ${hintColors.backgroundColor};
            color: ${hintColors.color};
        }
      }
    `;
  }, '');

const StyledGroup = styled.div.attrs({
  theme: getRawTheme
})`
  /* prettier-ignore */
  ${p => colorsStyle(p)}

  margin-bottom: 1rem;
`;

export default StyledGroup;
