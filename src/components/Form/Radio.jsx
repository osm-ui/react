import React from 'react';
import PropTypes from 'prop-types';

import Label from './Label';
import StyledRadio, { Wrapper } from './styles/Radio.style';

const Radio = ({ id, label, className, ...props }) => {
  return (
    <Wrapper className={className} {...props}>
      <StyledRadio id={id} {...props} />
      <Label htmlFor={id} {...props}>
        {label}
      </Label>
    </Wrapper>
  );
};

Radio.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  size: PropTypes.oneOf(['sm', 'md', 'lg']),
  label: PropTypes.string,
  disabled: PropTypes.bool,
  className: PropTypes.string
};

Radio.defaultProps = {
  label: '',
  size: 'md',
  disabled: false,
  className: ''
};

Radio.displayName = 'Form.Radio';
Radio.wrapperStyle = Wrapper;
Radio.style = StyledRadio;

export default Radio;
