import React from 'react';
import { storiesOf } from '@storybook/react';
import { host } from 'storybook-host';
import { withKnobs, boolean, select, number } from '@storybook/addon-knobs';

import defaultHostOptions from 'helpers/__stories__/defaultHostOptions';
import KnobsAlert from 'helpers/__stories__/components/KnobsAlert';
import FakeApp from 'helpers/__stories__/components/FakeApp';
import FakeNotificationCenter from 'helpers/__stories__/components/FakeNotificationCenter';

import {
  DefaultTheme,
  WhiteTheme,
  LightGrayTheme,
  DarkGrayTheme,
  AnthraciteTheme,
  YellowTheme,
  OrangeTheme,
  BrownTheme,
  RedTheme,
  RoseTheme,
  PurpleTheme,
  BlueTheme,
  SkyTheme,
  TurquoiseTheme,
  GreenTheme,
  Notification
} from 'index';

storiesOf('Notification', module)
  .addDecorator(withKnobs)
  .addDecorator(
    host({
      ...defaultHostOptions,
      title: 'Notification'
    })
  )
  .addWithInfo('Default state', () => (
    <DefaultTheme>
      <FakeApp fakeText>
        <FakeNotificationCenter>
          <Notification id={1}>This is a notification</Notification>
        </FakeNotificationCenter>
      </FakeApp>
    </DefaultTheme>
  ))
  .addWithInfo('Position', () => (
    <DefaultTheme>
      <FakeApp fakeText>
        <FakeNotificationCenter>
          <Notification id={11} position='top-right'>
            This is top-right position
          </Notification>
          <Notification id={12} position='top-left'>
            This is top-left position
          </Notification>
          <Notification id={13} position='bottom-right'>
            This is bottom-right position
          </Notification>
          <Notification id={14} position='bottom-left'>
            This is a bottom-left position
          </Notification>
        </FakeNotificationCenter>
      </FakeApp>
    </DefaultTheme>
  ))
  .addWithInfo('Direction', () => (
    <DefaultTheme>
      <FakeApp fakeText>
        <FakeNotificationCenter>
          <Notification id={11} timespan={4000} direction='horizontal'>
            This notification is horizontal
          </Notification>
          <Notification
            id={12}
            timespan={4000}
            position='bottom'
            direction='vertical'
          >
            This notification is vertical
          </Notification>
        </FakeNotificationCenter>
      </FakeApp>
    </DefaultTheme>
  ))
  .addWithInfo('Group', () => (
    <DefaultTheme>
      <FakeApp fakeText>
        <FakeNotificationCenter>
          <Notification.Group position='top-right'>
            <Notification id={101}>
              This is a notification with a lot of text. So i'll just sing a
              song. "In the town where i was born lived a maaAAAAaan who sailed
              to sea. And he toooold us of his life in a yeeellow submarine. We
              all live in a yellow submarine ! Yellow submarine ! Yellow
              submarine !"
            </Notification>
            <Notification id={102}>This is a notification</Notification>
            <Notification id={103}>This is a notification</Notification>
            <Notification id={104}>This is a notification</Notification>
          </Notification.Group>
        </FakeNotificationCenter>
      </FakeApp>
    </DefaultTheme>
  ))
  .addWithInfo('Delay', () => (
    <DefaultTheme>
      <FakeApp fakeText>
        <FakeNotificationCenter>
          <Notification.Group position='top-right'>
            <Notification id={101} timespan={2000}>
              This is a 2s notification
            </Notification>
            <Notification id={102} timespan={3000}>
              This is a 3s notification
            </Notification>
            <Notification id={103} timespan={4000}>
              This is a 4s notification
            </Notification>
            <Notification id={104} timespan={5000}>
              This is a 5s notification
            </Notification>
          </Notification.Group>
        </FakeNotificationCenter>
      </FakeApp>
    </DefaultTheme>
  ))
  .addWithInfo('Context', () => (
    <DefaultTheme>
      <FakeApp fakeText>
        You can choose a context for your notification.
        <FakeNotificationCenter>
          <Notification.Group position='top-right'>
            <Notification id={101} context='info'>
              This is an information notification
            </Notification>
            <Notification id={102} context='success'>
              This is a success notification
            </Notification>
            <Notification id={103} context='warning'>
              This is a warning notification
            </Notification>
            <Notification id={104} context='danger'>
              This is a danger notification
            </Notification>
          </Notification.Group>
        </FakeNotificationCenter>
      </FakeApp>
    </DefaultTheme>
  ))
  .addWithInfo('Pending', () => (
    <DefaultTheme>
      <FakeApp fakeText>
        <FakeNotificationCenter>
          <Notification.Group>
            <Notification id={1} pending>
              This is a notification pending for some action
            </Notification>
            <Notification id={2}>
              This is a notification that will disappear shortly
            </Notification>
          </Notification.Group>
        </FakeNotificationCenter>
      </FakeApp>
    </DefaultTheme>
  ))
  .addWithInfo('Call To Actions', () => (
    <DefaultTheme>
      <FakeApp fakeText>
        <FakeNotificationCenter>
          <Notification
            id={2}
            callToActions={[
              {
                text: 'CTA1',
                action: () => null
              },
              {
                text: 'CTA2',
                action: () => null
              }
            ]}
          >
            A notification with call-to-actions
          </Notification>
        </FakeNotificationCenter>
      </FakeApp>
    </DefaultTheme>
  ))
  .addWithInfo('Playground', () => {
    const position = select(
      'Position',
      ['top-left', 'top-right', 'bottom-right', 'bottom-left', 'top', 'bottom'],
      'top-left'
    );
    const direction = select(
      'Direction',
      ['horizontal', 'vertical'],
      'horizontal'
    );
    const pending = boolean('Is Pending', false);
    const context = select('Context', Notification.contexts, 'info');

    const timespanOptions = {
      range: true,
      min: 1000,
      max: 10000,
      step: 100
    };
    const timespan = number('Timespan', 2000, timespanOptions);

    const theme = select(
      'Theme',
      [
        'Default',
        'White',
        'LightGray',
        'DarkGray',
        'Anthracite',
        'Yellow',
        'Orange',
        'Brown',
        'Red',
        'Rose',
        'Purple',
        'Blue',
        'Sky',
        'Turquoise',
        'Green'
      ],
      'Turquoise'
    );
    const themes = {
      DefaultTheme,
      WhiteTheme,
      LightGrayTheme,
      DarkGrayTheme,
      AnthraciteTheme,
      YellowTheme,
      OrangeTheme,
      BrownTheme,
      RedTheme,
      RoseTheme,
      PurpleTheme,
      BlueTheme,
      SkyTheme,
      TurquoiseTheme,
      GreenTheme
    };
    const ThemeElement = themes[`${theme}Theme`];

    return (
      <DefaultTheme>
        <KnobsAlert />
        <FakeApp fakeText style={{ top: '80px' }}>
          <FakeNotificationCenter>
            <ThemeElement>
              <Notification
                id={17}
                position={position}
                direction={direction}
                context={context}
                pending={pending}
                timespan={timespan}
                // onOpen={action('onOpen')}
                // onClose={action('onClose')}
                // onClickClose={action('onClickClose')}
              >
                This is a notification
              </Notification>
            </ThemeElement>
          </FakeNotificationCenter>
        </FakeApp>
      </DefaultTheme>
    );
  });
