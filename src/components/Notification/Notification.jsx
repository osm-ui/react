import React from 'react';
import PropTypes from 'prop-types';
import Transition from 'react-transition-group/Transition';
import classnames from 'classnames';

import Button from 'components/Button';

import StyledNotification, { Wrapper, contexts } from './Notification.style';

class Notification extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      opened: true,
      closing: false,
      timeout: null
    };

    this.notif = null;

    this.setNotifRef = element => {
      this.notif = element;
    };
  }

  componentDidMount() {
    this.notif.style.height = `${this.notif.offsetHeight}px`;
  }

  close = () => {
    this.setState({ opened: false });
  };

  setTimeout = () => {
    this.setState({
      timeout: setTimeout(this.close, this.props.timespan)
    });
  };

  clearTimeout = () => {
    this.setState({
      timeout: clearTimeout(this.state.timeout)
    });
  };

  render() {
    const {
      callToActions,
      onClose,
      children,
      position,
      direction,
      pending,
      theme,
      className
    } = this.props;

    const asideClasses = classnames(className, 'notification');

    const wrapperClasses = classnames({
      [`position-${position}`]: true,
      [`direction-${direction}`]: true
    });

    const transitionStyles = {
      entered: {
        transform: 'translate(0, 0)'
      }
    };

    const closingTransitionStyles = {
      exiting: {
        minHeight: 0,
        height: 0,
        padding: 0,
        margin: 0
      }
    };

    const ctas = callToActions.map((cta, index) => (
      <Button key={index} size='xs' shape='square' onClick={cta.action}>
        {cta.text}
      </Button>
    ));

    return (
      <Transition
        in={this.state.opened}
        timeout={500}
        appear
        onEntered={pending ? () => null : this.setTimeout}
        onExited={() => this.setState({ closing: true })}
      >
        {state1 => (
          <Wrapper
            className={wrapperClasses}
            style={transitionStyles[state1]}
            theme={theme}
          >
            <Transition
              in={!this.state.closing}
              timeout={500}
              unmountOnExit
              onExited={onClose}
            >
              {state2 => (
                <StyledNotification
                  key='notification'
                  {...this.props}
                  className={asideClasses}
                  style={closingTransitionStyles[state2]}
                  onMouseEnter={this.clearTimeout}
                  onMouseLeave={pending ? () => null : this.setTimeout}
                  innerRef={this.setNotifRef}
                >
                  <div className='content'>
                    {children}
                    <div>{ctas}</div>
                  </div>
                  {!pending && (
                    <button className='close-btn' onClick={() => this.close()}>
                      <i className='fas fa-times' />
                    </button>
                  )}
                </StyledNotification>
              )}
            </Transition>
          </Wrapper>
        )}
      </Transition>
    );
  }
}

Notification.contexts = contexts;

Notification.propTypes = {
  id: PropTypes.number.isRequired,
  children: PropTypes.node.isRequired,
  onClose: PropTypes.func,
  context: PropTypes.oneOf(contexts),
  position: PropTypes.oneOf([
    'top-left',
    'top-right',
    'bottom-right',
    'bottom-left',
    'top',
    'bottom'
  ]),
  direction: PropTypes.oneOf(['horizontal', 'vertical']),
  callToActions: PropTypes.array,
  pending: PropTypes.bool,
  timespan: PropTypes.number,
  className: PropTypes.string
};

Notification.defaultProps = {
  context: 'info',
  position: 'top-right',
  direction: 'horizontal',
  callToActions: [],
  pending: false,
  timespan: 4000,
  className: ''
};

Notification.displayName = 'Notification';
Notification.style = Wrapper;

export default Notification;
