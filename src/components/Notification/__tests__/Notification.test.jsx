import React from 'react';
import 'jest-styled-components';
import { mount } from 'enzyme';
import { snapshotWithElementChildren } from 'helpers/tests';

import Button from 'components/Button';
import Notification from '..';

describe('When using snapshots', () => {
  it('Should render with an element children', () =>
    snapshotWithElementChildren(Notification, { id: 1 }));
});

describe('When testing initialization', () => {
  it('Should render with the correct default classnames', () => {
    const wrapper = mount(
      <Notification id={1}>
        <div />
      </Notification>
    );
    const notificationWrapper = wrapper.find(Notification.style);
    const aside = wrapper.find('aside');

    expect(aside.hasClass('notification')).toBe(true);
    expect(notificationWrapper.hasClass('position-top-right')).toBe(true);
    expect(notificationWrapper.hasClass('direction-horizontal')).toBe(true);
  });

  it('Should render with the correct classnames', () => {
    const wrapper = mount(
      <Notification
        id={1}
        className='test'
        position='bottom-left'
        direction='vertical'
      >
        <div />
      </Notification>
    );
    const notificationWrapper = wrapper.find(Notification.style);
    const aside = wrapper.find('aside');

    expect(aside.hasClass('test')).toBe(true);
    expect(aside.hasClass('notification')).toBe(true);
    expect(notificationWrapper.hasClass('position-bottom-left')).toBe(true);
    expect(notificationWrapper.hasClass('direction-vertical')).toBe(true);
  });

  it('Should render with ctas', () => {
    const wrapper = mount(
      <Notification
        id={1}
        callToActions={[{ text: 'CTA', action: () => null }]}
      >
        <div />
      </Notification>
    );

    const cta = wrapper.find(Button);

    expect(cta.length).not.toBe(0);
    expect(cta.text()).toBe('CTA');
  });

  it('Should be pending when given the pending prop', () => {
    jest.useFakeTimers();
    mount(
      <Notification id={1} pending>
        <div />
      </Notification>
    );

    jest.runAllTimers();

    expect(setTimeout).toHaveBeenCalledTimes(1);
  });
});

describe('When testing the callbacks', () => {
  it('Should set state.opened to false when clicking on close button', () => {
    const wrapper = mount(
      <Notification id={1}>
        <div />
      </Notification>
    );

    wrapper.find('.close-btn').simulate('click');
    expect(wrapper.state().opened).toBe(false);
  });

  it('Should call the onClose callback when closed', () => {
    jest.useFakeTimers();
    const onClose = jest.fn();
    mount(
      <Notification id={1} onClose={onClose}>
        <div />
      </Notification>
    );

    jest.runAllTimers();

    expect(onClose).toHaveBeenCalled();
  });
});

describe('When testing the timings', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });
  afterEach(() => {
    jest.clearAllTimers();
  });

  it('Should close after 4s', () => {
    mount(
      <Notification id={1}>
        <div />
      </Notification>
    );
    jest.runAllTimers();

    expect(setTimeout).toHaveBeenNthCalledWith(2, expect.any(Function), 4000);
  });

  it('Should suspend timeout when entered', () => {
    const wrapper = mount(
      <Notification id={1}>
        <div />
      </Notification>
    );

    wrapper
      .find('.notification')
      .at(0)
      .simulate('mouseenter');

    jest.runAllTimers();

    expect(clearTimeout).toHaveBeenCalledTimes(1);
  });

  it('Should reset timeout when left', () => {
    const wrapper = mount(
      <Notification id={1}>
        <div />
      </Notification>
    );
    const notification = wrapper.find('.notification').at(0);
    jest.advanceTimersByTime(1000);
    notification.simulate('mouseenter');
    jest.advanceTimersByTime(5000);
    notification.simulate('mouseleave');
    expect(setTimeout).toHaveBeenCalledTimes(3);
  });

  it('Should not reset timeout when left with pending prop', () => {
    const wrapper = mount(
      <Notification id={1} pending>
        <div />
      </Notification>
    );
    const notification = wrapper.find('.notification').at(0);
    notification.simulate('mouseenter');
    notification.simulate('mouseleave');
    jest.runAllTimers();
    expect(setTimeout).toHaveBeenCalledTimes(1);
  });
});
