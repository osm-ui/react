import React from 'react';
import 'jest-styled-components';
import { shallow } from 'enzyme';
import { snapshotWithElementChildren } from 'helpers/tests';
import Notification from '..';

describe('When using snapshots', () => {
  it('Should render with an element children', () =>
    snapshotWithElementChildren(Notification.Group));
});

describe('When testing initialization', () => {
  it('Should render with the correct default classnames', () => {
    const wrapper = shallow(
      <Notification.Group>
        <div />
      </Notification.Group>
    );

    expect(wrapper.hasClass('position-top-right')).toBe(true);
    expect(wrapper.hasClass('direction-horizontal')).toBe(true);
  });

  it('Should render with the correct default classnames', () => {
    const wrapper = shallow(
      <Notification.Group
        className='test'
        position='bottom-left'
        direction='vertical'
      >
        <div />
      </Notification.Group>
    );

    expect(wrapper.hasClass('test')).toBe(true);
    expect(wrapper.hasClass('position-bottom-left')).toBe(true);
    expect(wrapper.hasClass('direction-vertical')).toBe(true);
  });
});
