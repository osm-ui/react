import styled from 'styled-components';

import { Wrapper } from './Notification.style';

const StyledGroup = styled.aside`
  position: absolute;
  display: flex;
  flex-direction: column;
  @media (max-width: 50rem) {
    width: 100%;
  }

  ${Wrapper} {
    position: relative;
    margin-top: 0;
  }

  &:empty {
    padding-top: 0;
    padding-bottom: 0;
  }

  &.position-top-right {
    top: 0;
    right: 0;
    justify-content: flex-end;

    ${Wrapper}:first-child {
      margin-top: 0.5rem;
    }
  }

  &.position-top-left {
    top: 0;
    left: 0;
    justify-content: flex-start;

    ${Wrapper}:first-child {
      margin-top: 0.5rem;
    }
  }

  &.position-bottom-right {
    bottom: 0;
    right: 0;
    justify-content: flex-end;
    flex-direction: column-reverse;
  }

  &.position-bottom-left {
    bottom: 0;
    left: 0;
    justify-content: flex-start;
    flex-direction: column-reverse;
  }

  &.position-top {
    top: 0;
    left: 0;
    right: 0;
    width: 50%;
    margin-left: auto;
    margin-right: auto;

    ${Wrapper}:first-child {
      margin-top: 0.5rem;
    }
  }

  &.position-bottom {
    bottom: 0;
    left: 0;
    right: 0;
    width: 50%;
    margin-left: auto;
    margin-right: auto;
    flex-direction: column-reverse;
  }

  .notif-item {
    position: relative;
    width: 100%;
  }
`;

export default StyledGroup;
