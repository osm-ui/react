import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { host } from 'storybook-host';
import { withKnobs, boolean, select } from '@storybook/addon-knobs';

import defaultHostOptions from 'helpers/__stories__/defaultHostOptions';
import KnobsAlert from 'helpers/__stories__/components/KnobsAlert';
import FakeApp from 'helpers/__stories__/components/FakeApp';

import {
  DefaultTheme,
  WhiteTheme,
  LightGrayTheme,
  DarkGrayTheme,
  AnthraciteTheme,
  YellowTheme,
  OrangeTheme,
  BrownTheme,
  RedTheme,
  RoseTheme,
  PurpleTheme,
  BlueTheme,
  SkyTheme,
  TurquoiseTheme,
  GreenTheme,
  Titlebar
} from 'index';

const fakeAppStyle = {
  paddingTop: '5rem',
  paddingBottom: '5rem'
};

storiesOf('Titlebar', module)
  .addDecorator(withKnobs)
  .addDecorator(
    host({
      ...defaultHostOptions,
      title: 'Titlebar'
    })
  )
  .addWithInfo('Default state', () => (
    <DefaultTheme>
      <FakeApp style={fakeAppStyle}>
        <AnthraciteTheme>
          <Titlebar size='md'>Application title</Titlebar>
        </AnthraciteTheme>
      </FakeApp>
    </DefaultTheme>
  ))
  .addWithInfo('Non floating', () => (
    <DefaultTheme>
      <FakeApp style={fakeAppStyle}>
        <AnthraciteTheme>
          <Titlebar floating={false}>Application title</Titlebar>
        </AnthraciteTheme>
      </FakeApp>
    </DefaultTheme>
  ))
  .addWithInfo('Non block', () => (
    <DefaultTheme>
      <FakeApp style={fakeAppStyle}>
        <AnthraciteTheme>
          <Titlebar block={false}>Application title</Titlebar>
        </AnthraciteTheme>
      </FakeApp>
    </DefaultTheme>
  ))
  .addWithInfo('Position', () => (
    <DefaultTheme>
      <FakeApp style={fakeAppStyle}>
        <AnthraciteTheme>
          <Titlebar position='top-center'>Application header</Titlebar>
          <Titlebar position='bottom-right' block={false}>
            Application footer
          </Titlebar>
        </AnthraciteTheme>
      </FakeApp>
    </DefaultTheme>
  ))
  .addWithInfo('Theme', () => (
    <DefaultTheme>
      <FakeApp style={fakeAppStyle}>
        <BlueTheme>
          <Titlebar>Application header</Titlebar>
        </BlueTheme>
        <OrangeTheme>
          <Titlebar position='bottom-center'>Application footer</Titlebar>
        </OrangeTheme>
      </FakeApp>
    </DefaultTheme>
  ))
  .addWithInfo('Size', () => (
    <DefaultTheme>
      <FakeApp style={fakeAppStyle}>
        <AnthraciteTheme>
          <Titlebar size='lg'>Application header</Titlebar>
          <Titlebar size='sm' position='bottom-center'>
            Application footer
          </Titlebar>
        </AnthraciteTheme>
      </FakeApp>
    </DefaultTheme>
  ))
  .addWithInfo('Playground', () => {
    const position = select(
      'Position',
      ['top', 'top-left', 'top-right', 'bottom', 'bottom-left', 'bottom-right'],
      'top'
    );
    const size = select('Size', ['xs', 'sm', 'md', 'lg'], 'md');
    const container = select('Container', ['parent', 'root'], 'parent');
    const floating = boolean('Floating', true);
    const block = boolean('Block', true);
    const maximized = boolean('Maximized', false);
    const theme = select(
      'Theme',
      [
        'Default',
        'White',
        'LightGray',
        'DarkGray',
        'Anthracite',
        'Yellow',
        'Orange',
        'Brown',
        'Red',
        'Rose',
        'Purple',
        'Blue',
        'Sky',
        'Turquoise',
        'Green'
      ],
      'Purple'
    );
    const themes = {
      DefaultTheme,
      WhiteTheme,
      LightGrayTheme,
      DarkGrayTheme,
      AnthraciteTheme,
      YellowTheme,
      OrangeTheme,
      BrownTheme,
      RedTheme,
      RoseTheme,
      PurpleTheme,
      BlueTheme,
      SkyTheme,
      TurquoiseTheme,
      GreenTheme
    };
    const ThemeElement = themes[`${theme}Theme`];

    return (
      <DefaultTheme>
        <KnobsAlert />
        <FakeApp style={{ ...fakeAppStyle, ...{ top: '80px' } }}>
          <ThemeElement>
            <Titlebar
              maximized={maximized}
              floating={floating}
              block={block}
              position={position}
              container={container}
              size={size}
              onMaximize={action('onMaximize titlebar')}
              onUnmaximize={action('onUnmaximize titlebar')}
            >
              Application title
            </Titlebar>
          </ThemeElement>
        </FakeApp>
      </DefaultTheme>
    );
  });
