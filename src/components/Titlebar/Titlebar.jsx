import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { Footer, Header, StyledTitlebar } from './Titlebar.style';

class Titlebar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      maximized: props.maximized
    };
  }

  componentDidMount() {
    if (this.props.maximized === true) {
      this._triggerCallback('onMaximize');
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      maximized: nextProps.maximized
    });

    if (this.props.maximized !== nextProps.maximized) {
      if (nextProps.maximized === true) {
        this._triggerCallback('onMaximize');
      } else {
        this._triggerCallback('onUnmaximize');
      }
    }
  }

  _triggerCallback = name => {
    if (this.props[name] !== null) {
      this.props[name]();
    }
  };

  render() {
    const {
      position,
      size,
      container,
      floating,
      block,
      className,
      children,
      ...rest
    } = this.props;

    const classes = classnames(className, {
      [size]: true,
      [`position-${position}`]: true,
      [`container-${container}`]: true,
      floating,
      block,
      maximized: this.state.maximized
    });

    const Wrapper = /^top-/.test(position) ? Header : Footer;

    return (
      <Wrapper className={classes}>
        <StyledTitlebar className={classes} {...rest}>
          {children}
        </StyledTitlebar>
      </Wrapper>
    );
  }
}

Titlebar.propTypes = {
  position: PropTypes.oneOf([
    'top-center',
    'top-left',
    'top-right',
    'bottom-center',
    'bottom-left',
    'bottom-right'
  ]),
  size: PropTypes.oneOf(['xs', 'sm', 'md', 'lg']),
  container: PropTypes.oneOf(['parent', 'root']),
  floating: PropTypes.bool,
  block: PropTypes.bool,
  maximized: PropTypes.bool,
  onMaximize: PropTypes.func,
  onUnmaximize: PropTypes.func,
  className: PropTypes.string,
  children: PropTypes.node
};

Titlebar.defaultProps = {
  position: 'top-center',
  size: 'md',
  container: 'parent',
  floating: true,
  block: true,
  maximized: false,
  onMaximize: null,
  onUnmaximize: null,
  className: '',
  children: ''
};

Titlebar.displayName = 'Titlebar';
Titlebar.headerStyle = Header;
Titlebar.footerStyle = Footer;
Titlebar.style = StyledTitlebar;

export default Titlebar;
