import React from 'react';
import 'jest-styled-components';
import { shallow, mount } from 'enzyme';
import { snapshotWithElementChildren } from 'helpers/tests';
import Modal from '..';
import { Header, Footer } from 'components/Layout';
import Loader from 'components/Loader';

describe('When using snapshots', () => {
  it('Should render with an element children', () =>
    snapshotWithElementChildren(Modal));
});

describe('When using snapshots with loader', () => {
  it('Should render with an element children', () =>
    snapshotWithElementChildren(Modal, {
      loading: true,
      loaderLabel: 'loading'
    }));
});

describe('When testing initialization', () => {
  it('Should render with the correct default classnames', () => {
    const wrapper = mount(<Modal />);

    const contentWrapper = wrapper.find('.content-wrapper').at(0);
    const content = wrapper.find('.content');

    expect(contentWrapper.hasClass('container-root')).toBe(true);
    expect(content.hasClass('loading')).toBe(false);
  });

  it('Should render with the correct classnames', () => {
    const wrapper = mount(
      <Modal className='test' container='parent' scrollContent loading />
    );

    const contentWrapper = wrapper.find('.content-wrapper').at(0);
    const content = wrapper.find('.content');

    expect(contentWrapper.hasClass('test')).toBe(true);
    expect(contentWrapper.hasClass('container-parent')).toBe(true);
    expect(contentWrapper.hasClass('scroll-content')).toBe(true);
    expect(content.hasClass('test')).toBe(false);
    expect(content.hasClass('loading')).toBe(true);
  });

  it('Should render with header', () => {
    const wrapper = mount(<Modal header='test' />);
    const header = wrapper.find(Header);

    expect(header.text()).toBe('test');
  });

  it('Should render with title', () => {
    const wrapper = mount(<Modal title='test' />);
    const header = wrapper.find(Header);

    expect(header.text()).toBe('test');
  });

  it('Should render with header and title', () => {
    const wrapper = mount(<Modal title='test' header='test' />);
    const header = wrapper.find(Header);

    expect(header.text()).toBe('testtest');
  });

  it('Should render with footer', () => {
    const wrapper = mount(<Modal footer='test' />);
    const footer = wrapper.find(Footer);

    expect(footer.text()).toBe('test');
  });

  it('Should render with loader', () => {
    const wrapper = mount(<Modal loading />);
    const loader = wrapper.find(Loader);

    expect(loader.length).not.toBe(0);
  });

  it('Should render with loader and label', () => {
    const wrapper = mount(<Modal loading loaderLabel='test' />);
    const loader = wrapper.find(Loader);

    expect(loader.prop('label')).toBe('test');
  });

  it('Should not render header or footer with loader', () => {
    const wrapper = mount(<Modal loading header='test' footer='test' />);
    const header = wrapper.find(Header);
    const footer = wrapper.find(Footer);

    expect(header.length).toBe(0);
    expect(footer.length).toBe(0);
  });
});

describe('When testing the callbacks', () => {
  it('Should call the onOpen callback when component is mounted', () => {
    const onOpen = jest.fn();
    shallow(<Modal onOpen={onOpen} />);
    expect(onOpen).toHaveBeenCalledTimes(1);
  });

  it('Should call the onClose callback when component is unmounted', () => {
    const onClose = jest.fn();
    const wrapper = shallow(<Modal onClose={onClose} />);
    wrapper.unmount();
    expect(onClose).toHaveBeenCalledTimes(1);
  });
});
