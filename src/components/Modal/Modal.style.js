import styled from 'styled-components';

import { getRawTheme } from 'helpers/themes';
import { Header, Footer } from 'components/Layout';

const StyledModal = styled.aside.attrs({
  theme: getRawTheme
})`
  position: absolute;
  z-index: 1000;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  padding: ${p => p.theme.modal.overlayPadding};
  background: ${p => p.theme.modal.overlayBackgroundColor};
  transition: opacity 300ms ease-in-out;
`;

export const ContentWrapper = styled.div.attrs({
  theme: getRawTheme
})`
  position: relative;
  min-height: 10rem;
  max-height: 100%;
  padding: ${p => p.theme.modal.padding};
  background: ${p => p.theme.modal.backgroundColor};
  color: ${p => p.theme.modal.color};
  overflow-y: auto;
  opacity: 0;
  transition: opacity 300ms ease-in-out;

  &.scroll-content {
    display: flex;
    flex-direction: column;
    overflow-y: none;
  }

  &.scroll-content ${Header.style} {
    margin-top: -1rem;
    margin-bottom: 1rem;
  }

  .content {
    padding: 1rem;
  }

  &.scroll-content .content {
    overflow-y: auto;
    margin: 0;
    border-color: ${p => p.theme.borderColor};
    border-style: ${p => p.theme.borderStyle};
    border-width: 1px 0 1px 0;

    &::after {
      content: '';
      margin-top: 1rem;
      display: block;
    }
  }

  .content.loading {
    display: none;
  }

  &.scroll-content ${Footer.style} {
    margin-top: 1rem;
    margin-bottom: -1rem;
  }
`;

export default StyledModal;
