import React from 'react';
import 'jest-styled-components';
import { shallow } from 'enzyme';
import { snapshotWithElementChildren } from 'helpers/tests';
import Button from '..';

describe('When using snapshots', () => {
  it('Should render with an element children', () =>
    snapshotWithElementChildren(Button));

  it('Should render an anchor typed Button with an element children', () =>
    snapshotWithElementChildren(Button, { type: 'anchor' }));
});

describe('When using an anchor typed Button', () => {
  it('Should get the anchor prop', () => {
    const wrapper = shallow(<Button type='anchor' />);

    expect(wrapper.prop('role')).toBe('button');
  });
});

describe('When testing classnames', () => {
  it('Should render with correct default classnames', () => {
    const wrapper = shallow(<Button />);

    expect(wrapper.hasClass('btn')).toBe(true);
    expect(wrapper.hasClass('shape-square')).toBe(true);
    expect(wrapper.hasClass('btn-default')).toBe(true);
    expect(wrapper.hasClass('btn-md')).toBe(true);
    expect(wrapper.hasClass('btn-block')).toBe(false);
    expect(wrapper.hasClass('disabled')).toBe(false);
  });

  it('Should render with correct classnames', () => {
    const props = {
      context: 'primary',
      shape: 'round',
      size: 'sm',
      block: true,
      active: true,
      disabled: true
    };

    const wrapper = shallow(<Button className='test' {...props} />);

    expect(wrapper.hasClass('test')).toBe(true);
    expect(wrapper.hasClass('shape-round')).toBe(true);
    expect(wrapper.hasClass('btn-primary')).toBe(true);
    expect(wrapper.hasClass('btn-sm')).toBe(true);
    expect(wrapper.hasClass('btn-block')).toBe(true);
    expect(wrapper.hasClass('disabled')).toBe(true);
  });
});
